<?php
/**
 * This file includes all of the additional functions used in the ShareIT websites.
 */
namespace ShareIt;

/**
 * Returns the pictureID if its set
 */
function set_pictureID() {
	if (isset($_GET["pictureID"])) 
		return$_GET["pictureID"];
}
/**
 * Returns the propertyID if its set
 */
function set_propertyID() {
	if (isset($_GET["propertyID"])) 
		return$_GET["propertyID"];
}
/**
 * Returns the loactionID if its set
 */
function set_locationID() {
	if (isset($_GET["locationID"])) 
		return$_GET["locationID"];
}
/**
 * Returns the loactionID if its set
 */
function set_countrycode() {
	if (isset($_GET["countrycode"])) 
		return$_GET["countrycode"];
}
/**
 * Returns the delete flag if its set
 */
function delete_flag() {
	if (isset($_GET["delete"])) 
		return$_GET["delete"];
}
/**
 * Returns the delete flag if its set
 */
function set_landlordID() {
	if (isset($_GET["landlordID"])) 
		return $_GET["landlordID"];
}

?>