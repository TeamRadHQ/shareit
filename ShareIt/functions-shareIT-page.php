<?php
/**
 * This file contains all of the functions for 
 * compiling data for page display. This file needs 
 * to be cleaned up and some functions need to be
 * moved or decluttered.
 * @todo Separate DB instructions into their own functions and move them to the database functions file. 
 */
namespace ShareIt;

function template () {
	return new \Mustache_Engine(array(
		'loader' => new \Mustache_Loader_FilesystemLoader(__DIR__.'/../TeamRad/Form/templates/'),
	));  
}

/**
 * Adds the Body header.
 * @param string $title The title you want to set for 
 * the page.
 */
function body_header($title) {
	$body_header = '<body class="container">';
	$body_header.= '<header class="page-header"><h2>'.$title.'</h2></header>';
	return $body_header;
}

/**
 * Adds the required JavaScript for this site 
 * using Mustache templtes. 
 * @param string $js Use this to add additional custom JavaScript for this page. It will be appended after main scripts. 
 */
function add_JS($js="") {
	echo nav();
	$js = [];
	$mustache = template();
	echo $mustache->render('page_js', $js);
}

/**
 * Adds the HTML page header.
 * @param String $title The page title.
 */
function add_head($title) {
	$page_head["title"] = $title;
	$mustache = template();
	echo $mustache->render('page_head', $page_head);
	echo body_header($title);
}

function add_footer() {
	$mustache = template();
	echo $mustache->render('page_footer', array() );
}

/**
 * Returns an success message on completeion of a database update or insert query.
 * To flag a success, set an insert or update get flag with the redirect url.
 * @param  string $update A message to display upon a successful database update.
 * @param  string $insert A message to display upon a successful database insert.
 * @return echo         html message alert
 */
function success_message($update="", $insert="", $delete="") {
	// Show a success message if data was inserted or updated
	if (isset($_GET["update"]) || isset($_GET["insert"]) || isset($_GET["delete"])  ) {
		?>

		<div class="alert alert-success alert-dismissible" role="alert">
		  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		  <p><strong>Success! </strong><?php
		// Print the update message if the data was updated
		if (isset($_GET["update"])) 
		 echo $update;
		// Print the insert message if the data was inserted.
		if (isset($_GET["insert"])) 
			echo $insert;
		// Print the delete message if the data was deleted.
		if (isset($_GET["delete"])) 
			echo $delete;
	?>
		  </p>
		  <p><a href="admin.php" class="alert-link">Back to Admin Page</a></p>
		</div><?php 
	} // end if(isset)
}// end success_message()


/**
 * Returns a table of properties for a Landlord user.
 * @return html A table of properties for this landlord.
 */
function profile_table() {
	$db = new DB;
	$sql = "SELECT firstname, 
				lastname, 
				mob1, 
				mob2, 
				email, 
				username,
				countryphonecode,
				lo.locationname as location,
				r.role,
				c.commonname as country
			FROM landlord l, user u, role r, country c, location lo
			WHERE u.userID = ". $_SESSION["userID"]."
				AND l.landlordID = u.userID
				AND u.roleID = r.roleID
				AND l.CountryCode = c.countrycode
				AND l.locationID = lo.locationID
				";
	$results = $db->query($sql);
	// Get the user
	$user = $results->fetch(DB::FETCH_ASSOC);
	$mustache = template();
	echo $mustache->render('table_profile', $user);
} // End profile_table()

/**
 * Gets the image data from the database, adds the 
 * propertyID and sends to Mustache.
 * @return html A collection of linked images for editing.
 */
function admin_property_img() {
	// Return if there's no propertyID
	if( !isset( $_GET["propertyID"] ) ) return;
	// Otherwise, get the images and set the propertyID.
	$images["rows"] = get_property_images();
	$images["propertyID"] = $_GET["propertyID"];
	$mustache = template();
	echo $mustache->render('admin_property_img', $images);
}

/**
 * Returns an Image count
 * @param  Integer 	$propertyID The id of the property to get a count for.
 * @return Integer             	The number of images already added to the property.
 */
function img_count($propertyID) {
	$db = new DB;
	$query = "SELECT COUNT(*) as count FROM picture WHERE propertyID = $propertyID";
	$result = $db->query($query);
	$data = $result->fetch(DB::FETCH_ASSOC);
	return $data["count"];
}
/**
 * Returns an Image count
 * @param  Integer 	$propertyID The id of the property to get a count for.
 * @return Integer             	The number of images already added to the property.
 */
function featured_img($propertyID) {
	$db = new DB;
	$query = "SELECT photoname as href FROM picture WHERE propertyID = $propertyID AND featuredImage = 1";
	$result = $db->query($query);
	$data = $result->fetch(DB::FETCH_ASSOC);
	return $data["href"];
}
/**
 * Gets the image with pictureID
 * @param  integer $pictureID
 * @return string             The file name
 */
function single_img($pictureID) {
	$db = new DB;
	$query = "SELECT photoname as href FROM picture WHERE pictureID = $pictureID";
	$result = $db->query($query);
	$data = $result->fetch(DB::FETCH_ASSOC);
	return $data["href"];
}
/**
 * Returns the name of the image with pictureID
 * @param  integer $pictureID 	The pictureID whose name you want to get.
 * @return string            	The picture's file name.
 */
function img_name($pictureID) {
	// Get global database connection
	$db = new DB;
	// A query for selecting image data
	$sql = "SELECT photoname FROM picture WHERE pictureID = $pictureID";
	// Database results
	$results = $db->query($sql);
	$row = $results->fetch(DB::FETCH_ASSOC);
	return $row["photoname"];
}

/**
 * Gets a property's image data from the database and 
 * returns it as an array.
 * @param  integer $propertyID The ID of the property to get images for.
 * @return array              An array of image data.
 */
function get_property_images($propertyID=0) {
	// If no propertyID is passed, check for $_GET data
	if ( !$propertyID && isset($_GET["propertyID"]) ) {
		$propertyID = $_GET["propertyID"];
	}
	// Stop if there is no propertyID
	if(!$propertyID)
		return;
	// An array for storing image data
	$images = [];
	// Get global database connection
	$db = new DB;
	// A query for selecting image data
	$sql = "SELECT propertyID, photoName as file, comments, pictureID, featuredImage FROM picture WHERE propertyID = $propertyID ORDER BY featuredImage DESC";
	// Database results
	$results = $db->query($sql);
	// Add each row to images array
    $i=0;
    while($row = $results->fetch(DB::FETCH_ASSOC)) {
    	// Get the image
    	$image = $row; 
    	// Add the slide number.
		$image["slide"] = $i;
    	// Set featured image to active.
    	if ($image["featuredImage"])
    		$image["active"] = true;
    	// Add the image to the array.
    	$images[] = $image;
    	$i++;
    }
    return $images;
}

/**
 * Returns a count of properties for a user.
 * @return integer The number of properties.
 */
function user_property_count($landlordID="") {
	$db = new DB;
	if (!$landlordID) $landlordID = $_SESSION["userID"];
	// See if there are properties in the database
	$sql = "SELECT COUNT(*) as propertyCount FROM property WHERE landlordID = ". $landlordID;
	$results = $db->query($sql);
	$count = $results->fetch(DB::FETCH_ASSOC);
	return $count["propertyCount"];
}
/**
 * Gets the property data and sends it to Mustache.
 * @return html A table listing properties.
 */
function property_table($properties=null) {
	// If no property list is supplied, get the admin properties.
	if (!$properties) $properties = admin_properties();
	// Pass the properties to Mustache template
	$mustache = template();
	echo $mustache->render('table_admin_property', $properties);
	// return $mustache->render('table_admin_property', $table);
}
function admin_properties() {
	$properties = landlord_properties($_SESSION["userID"]);
	// Add the admin actions flag
	$properties["actions"] = 1;
	return $properties;
}


/**
 * Sets all of the navigation data and sends it to 
 * Mustache for templating. It checks to see if a 
 * user is logged in and will either add a user 
 * menu or a login menu.
 * @return html The ShareIt website nav bar.
 */
function nav() {
	$nav["main_menu"][] = nav_item('Home','index.php');
	$nav["main_menu"][] = nav_item('Locations','location.php');
	if (isset($_SESSION["userID"])) {
		$nav["user_menu"]["items"][] = nav_item('My Properties','admin.php');
		$nav["user_menu"]["items"][] = nav_item('New Property','edit.php');
		$nav["user_menu"]["items"][] = nav_item('My Profile','profile.php');
	} else {
		$nav["login_menu"] = true;
	}
	$mustache = template();
	return $mustache->render('nav', $nav);
}
/**
 * This function is used for preparing navigation
 * items for templating. It compres the current page
 * to the $href and if it finds a match applies an 
 * active class for display purposes.
 * @param  string $title The text to display on the page.
 * @param  string $href  The page url relative to the root.
 * @return array         An array containing the nav item data.
 */
function nav_item($title, $href) {
	$item["title"] = $title;
	$item["href"] = $href;
    $page = basename($_SERVER['PHP_SELF']);
	if ($page == $href)
		$item["class"] = "active";
	return $item;
}

?>