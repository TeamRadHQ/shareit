<?php
namespace ShareIt;
/**
 * This file contains the global Mustache Template object for use by the ShareIt website. 
 */
\Mustache_Autoloader::register();
/**
 * This function should be used any time a Mustache object is 
 * instantiated.
 * @param  string $dirname The directory path to the Mustache templates you want to use.
 * @return \Mustache\Mustache          A new Mustache Engine object.
 */
function mustache($dirname=null) {
	if(!$dirname)
	$mustache = new \Mustache_Engine(array(
			'loader' => new \Mustache_Loader_FilesystemLoader('/home/bqeflonwei/shareit.teamradhq.com/TeamRad/Form/templates'),
		));  
	return $mustache;
}
/**
 * This is the same as the mustache function except that the path is hardcoded. 
 * @return \Mustache\Mustache          A new Mustache Engine object.
 */
function mustache_harcode() {
	$mustache = new \Mustache_Engine(array(
			'loader' => new \Mustache_Loader_FilesystemLoader('/home/bqeflonwei/shareit.teamradhq.com/TeamRad/Form/templates'),
		));  
	return $mustache;
}
?>
