<?php
/**
 * ShareIT database functions are contained here. 
 * This file tries to get a new global database object
 * which can be used throughout the project. 
 */
namespace ShareIt;
// Database connection
try {
	$db = new DB();
	// set the DB error mode to exception
	$db->setAttribute(DB::ATTR_ERRMODE, DB::ERRMODE_EXCEPTION);
} catch(PDOException $e) {
	echo "Failed: ", $e->getMessage();
}
/**
 * Sets global login variables if login form has been submitted. 
 * This function will probably be replaced with better functionality.
 */
function set_login_vars() {
	$GLOBALS["email"] = '';
	$GLOBALS["password"] = '';
	if( isset($_POST["email"]) && isset($_POST["password"])) {
		$GLOBALS["email"] = $_POST["email"];
		$GLOBALS["password"] = $_POST["password"];
	}
}
/**
 * This function redirects a public user to the login page. It is 
 * used to control access to administrative areas of the site. If the 
 * user has an an active session, they will be allowed to view the page. 
 * If the user doesn't have a session, they will be redirected to login.	
 * @return redirect Redirects unauthenticated users.
 */
function redirect_login() {
	// Check if the userID is not set or does not exist
	if( !isset($_SESSION["userID"]) || !$_SESSION["userID"]) {
		// Redirect the user to login
		header("Location: http://shareit.org/login.php");
		die();
	}
}
/**
 * Checks for POST data and matches it against user database
 * @return array Returns an array of user data or false. 
 */
function check_login() {
	// Set the loging vars
	set_login_vars();
	// Check if the user is set
	if ( !isset($_POST["email"]) && !isset($_POST["password"]) )
		return false;
	// get DB connection
	$db = new DB;
	global $email;
	global $password;
	// Set the variables from login form
	$form_email = $_POST["email"];
	$form_password = $_POST["password"];
	// Select statement to get user information
	$sql = "SELECT * FROM user WHERE email = '$email'";
	$result = $db->query($sql);
	// Set the result to array
	$user = $result->fetch(DB::FETCH_ASSOC);
	// Check if a user was found.
	if (!$user) 
		// There is no user so fail.
		return false;
	// Check if password matches.	
	if ($user["password"] === $password) {
		// Password was matched so return the user.
		return $user; 
	} else {
		// Password wasn't matched so fail.
		return false;
	}
} // End check_login


function delete_property($propertyID) {
	// queries for deleting a property and its images.
	$delete_property = "DELETE FROM property WHERE propertyID = $propertyID";
	$delete_images = "DELETE FROM picture WHERE propertyID = $propertyID";
	// Get the global database connection
	$db = new DB;
	// Run the DELETE queries
	$db->exec($delete_property);
	$db->exec($delete_images);
	// Now set a string to match the images in the uploads folder (eg p20- for propertyID 20)
	$img = "p".$propertyID."-";
	// Delete each image in the folder 
	foreach (glob("uploads/*".$img."*") as $filename) {
    	unlink($filename);
	}
	// Records deleted
	return true;
}
function delete_img($pictureID, $href) {
	// query for deleting a an image.
	$delete_image = "DELETE FROM picture WHERE pictureID = $pictureID";
	// Get the global database connection
	$db = new DB;
	// Run the DELETE query
	$db->exec($delete_image);
	// Delete each image in the folder 
	$filename = "uploads/" . $href;
	unlink($filename);
	// Records deleted
	return true;
}

function location_name($locationID) {
	// return if there is no locationID
	if (!$locationID) return;
	$db = new DB;
	$sql = "SELECT locationname FROM location WHERE locationID = ".$locationID;
	$results = $db->query($sql);
	$results = $results->fetch(DB::FETCH_ASSOC);
	return $results["locationname"];
}
function country_name($countrycode) {
	// return if there is no country code
	if (!$countrycode) return;
	$db = new DB;
	$sql = "SELECT commonname AS country FROM country WHERE countrycode = '".$countrycode ."'";
	$results = $db->query($sql);
	$results = $results->fetch(DB::FETCH_ASSOC);
	return $results["country"];
}


/**
 * Gets the property data for the property which has propertyID.
 * @param  int $propertyID The property's ID
 * @return Array             An array of property data.
 */
function get_property($propertyID="") {
	if ( !$propertyID ) return;
	$sql = "SELECT p.propertyID, 
			p.landlordID,
			ln.firstname,
			ln.lastname,
			u.email,
			ln.mob1,
			ln.mob2,
			p.comments,
			p.costperday,
			p.street,
			p.suburb, 
			p.postcode,
			p.bedrooms,
			p.bedsingle,
			p.beddouble,
			p.kitchen,
			p.laundry,
			p.toilet,
			l.locationname as location,
			c.commonname as country,
			p.locationID,
			c.countrycode
		FROM property p, country c, location l, user u, landlord ln
		WHERE propertyID = " . $propertyID . "
			AND p.locationID = l.locationId
			AND p.landlordID = u.userID
			AND p.landlordID = ln.landlordID
			AND l.countryCode = c.countrycode";
	$db = new DB;
	$results = $db->query($sql);
	// Get the user
	$property = $results->fetch(DB::FETCH_ASSOC);
	return $property;
}
/**
 * This function gets a landlord's property data and returns it as an array.
 * @return array An array of property data for a landlord
 */
function landlord_properties($landlordID) {
	$db = new DB;
	// Get the property count
	$properties["count"] = user_property_count($landlordID);  
	// If there are properties get them
	if ($properties["count"]) {
		$sql = "SELECT p.propertyID, 
					c.countrycode,
					l.locationID,
					c.commonname as country,  
					l.locationName as location,
					p.comments as description,
					p.street,
					p.suburb,
					p.postcode,
					p.costperday as cost,
					p.bedSingle + p.bedDouble as bedTotal
				FROM property p, location l, country c
				WHERE landlordID = ". $landlordID."
					AND p.locationID = l.locationID
					AND l.countryCode = c.countryCode
				ORDER BY country, location";
		$results = $db->query($sql);
		// Add each property to the array
	    while($row = $results->fetch(DB::FETCH_ASSOC)) {
	    	$property = $row;
	    	$property["images"] = img_count($property["propertyID"]); 
			$property["featuredImg"] = featured_img($property["propertyID"]);    	
	    	// Add this property to the array
	    	$properties["rows"][] = $property; 
	    } // end while($results)
	}
	return $properties;
} // end landlord_properties()

function location_properties($locationID) {
	$db = new DB;
		$sql = "SELECT p.propertyID, 
					c.countrycode,
					l.locationID,
					c.commonname as country,  
					l.locationName as location,
					p.comments as description,
					p.street,
					p.suburb,
					p.postcode,
					p.costperday as cost,
					p.bedSingle + p.bedDouble as bedTotal
				FROM property p, location l, country c
				WHERE p.locationID = ". $locationID."
					AND p.locationID = l.locationID
					AND l.countryCode = c.countryCode
				ORDER BY country, location";
		$results = $db->query($sql);
		// Add each property to the array
	    while($row = $results->fetch(DB::FETCH_ASSOC)) {
	    	$property = $row;
	    	$property["images"] = img_count($property["propertyID"]); 
			$property["featuredImg"] = featured_img($property["propertyID"]);    	
	    	// Add this property to the array
	    	$properties["rows"][] = $property; 
	    } // end while($results)
	return $properties;
} // end locations_properties()
function random_properties($limit = 10) {
	$db = new DB;
		$sql = "SELECT p.propertyID, 
					c.countrycode,
					l.locationID,
					c.commonname as country,  
					l.locationName as location,
					p.comments as description,
					p.street,
					p.suburb,
					p.postcode,
					p.costperday as cost,
					p.bedSingle + p.bedDouble as bedTotal
				FROM property p, location l, country c
				WHERE p.locationID = l.locationID
					AND l.countryCode = c.countryCode
				ORDER BY RAND()
				LIMIT " . $limit;
		$db = new DB();
		$db->setAttribute(DB::ATTR_ERRMODE, DB::ERRMODE_EXCEPTION);
		$results = $db->query($sql);
		// Add each property to the array
	    while($row = $results->fetch(DB::FETCH_ASSOC)) {
	    	$property = $row;
	    	$property["images"] = img_count($property["propertyID"]); 
			$property["featuredImg"] = featured_img($property["propertyID"]);    	
	    	// Add this property to the array
	    	$properties["rows"][] = $property; 
	    } // end while($results)
	return $properties;
} // end locations_properties()
function country_properties($countrycode) {
	$db = new DB;
		$sql = "SELECT p.propertyID, 
					c.countrycode,
					l.locationID,
					c.commonname as country,  
					l.locationName as location,
					p.comments as description,
					p.street,
					p.suburb,
					p.postcode,
					p.costperday as cost,
					p.bedSingle + p.bedDouble as bedTotal
				FROM property p, location l, country c
				WHERE p.locationID = l.locationID
					AND l.countryCode = '". $countrycode ."'
					AND l.countryCode = c.countrycode
				ORDER BY country, location";
		$results = $db->query($sql);
		// Add each property to the array
	    while($row = $results->fetch(DB::FETCH_ASSOC)) {
	    	$property = $row;
	    	$property["images"] = img_count($property["propertyID"]); 
			$property["featuredImg"] = featured_img($property["propertyID"]);    	
	    	// Add this property to the array
	    	$properties["rows"][] = $property; 
	    } // end while($results)
	return $properties;
} // end locations_properties()
?>