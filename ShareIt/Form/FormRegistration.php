<?php
/**
 * This file contains the FormProfile class.
 */
namespace ShareIt\Form;
//
// Helpers
use \TeamRad\Helpers\Opt as Opt;
use \TeamRad\Helpers\Cnd as Cnd;
// Form FieldSets
// use \ShareIt\Form\Fieldsets\Profile as Profile;
use \ShareIt\Form\Fieldsets\ProfileName as ProfileName;
use \ShareIt\Form\Fieldsets\ProfileContact as ProfileContact;
use \ShareIt\Form\Fieldsets\ProfilePassword as ProfilePassword;
use \ShareIt\Form\Fieldsets\LocationPicker as ProfileLocation;

/**
 * FormProfile extends the Form class.
 * It is used to create a form for a landlord to update their profile
 * on the ShareIT website. 
 */
class FormRegistration extends Form {
	/**
	 * The constructor sets the userID from the website session. It then adds 
	 * fieldsets for the landlord's name, location and contact information.
	 * If the user hasn't submitted data, then it will collect this from the 
	 * ShareIT database and add it to the form. 
	 * If the user has submitted the form, the input data will be validated
	 * and the database will be updated if there are no errors. 
	 */
	public function __construct() {
		parent::__construct();
		$this->set_fieldset(new ProfilePassword());
		// Get the property data from the database if there is no $_POST data		
		if (count($_POST) < 1) {
			$user = $this->get_data();
			if (is_array($user))
				$this->set_values($user);
		} // end if(count)

		// Set the form action
		$this->set_action("register.php");
		// Set the post values
		$this->set_POST_values();

		// Complete registration by entering user data
		if (count($_POST) > 0 && $this->error_count < 1) {
			$this->insert_data();
		}
	}
	/**
	 * This method binds the form data then executes a database update query.	
	 * @return Redirect Redirects the user upon update.
	 */
	protected function insert_data() {
		$values = $this->prep_db_values();
		// Insert the record and get the id.
		$last_id = parent::insert_data($values);
	    // Insert a blank record into the landlord database
//		$insert = "INSERT INTO user (username, password, clue, email, roleID) VALUES (:username, :password, 0, :email, 2)";
	    $sql = "INSERT INTO landlord (landlordID, firstname, lastname, countrycode, locationID, mob1, mob2, countryphonecode)
	    		VALUES (".$last_id.", 'Enter your details', 'Enter your details', '00', 00, '0', '0', '0')";

	    $db = new DB;
	    $db->query($sql);
		// Redirect the user
		header("Location: http://shareit.org/login.php?registered=1");
		die();
	}
	/**
	 * This function prepares the form field input values so that they can be 
	 * bound to the insert and update database queries.
	 * @return Array An array of bound database field values.
	 */
	protected function prep_db_values() {
		$values = $this->get_values();
		$values = [
			":email" 		=> $values["email"],
			":username" 	=> $values["username"],
			":password" 	=> $values["password"],
		];
		return $values;
	}
	/**
	 * This method prepares the default queries for this form. 
	 * If a propertyID exists, it sets SELECT and UPDATE queries.
	 * If a propertyID does not exist, it sets a SELECT and INSERT query. 
	 */
	protected function form_queries() {
		// Set select query if there is a propertyID or false

		// Set the update query or false
		$update = 0;			
		$this->set_qry_update($update);
		// Set the insert query to false
		$insert = "INSERT INTO user (username, password, clue, email, roleID) VALUES (:username, :password, 0, :email, 2)";
		// INSERT INTO table_name (column1, column2, column3,...)
		// VALUES (value1, value2, value3,...)
		$this->set_qry_insert($insert);
	}
} // End class Form
