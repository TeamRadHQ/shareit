<?php
/**
 * This file containst the ShareIt Field class.
 */
namespace ShareIt\Form;
/**
 * The ShareIt Field class extends the TeamRad field 
 * class. It adds additional methods specifically
 * used on the ShareIt website. 
 */
class Field extends \TeamRad\Form\Field  {
	/**
	 * Constructs a new Field object. 
	 * An Id must be passed at construction. However, all other 
	 * arguments are optional. 
	 * @uses Field::set_id() to set field's id.
	 * @uses Field::set_required() to set if the field is required.
	 * @uses Field::set_type() to set the field's data type.
	 * @uses Field::set_label() to set and format the field's label.
	 * @uses Field::set_placeholder() to set and format the field's placeholder text.
	 * @uses Field::set_message() to set a user message for the field.
	 * @uses Field::set_conditions() to set a user conditions for the field.
	 * @uses Field::set_options() to set a user options for the field.
	 * @param string  $id          (Required) A unique identifier for this field
	 * @param boolean $required    Defaults to true, which indicates that the field is required.
	 * @param string  $type        The field's data type. Defaults to "text".
	 * @param string  $label       A label for this field. 
	 * @param string  $placeholder An input placeholder for this field.
	 * @param array   $conditions  An array of conditions for data validation.
	 * @param array   $options     An array of option value pairs for selection.
	 * @param string  $message     An information message about this field to display to the user.
	 */
	public function __construct(
		$id, $required=true, $type='text', 
		$label=null, $placeholder=null, 
		$conditions=array(), $options=array(), 
		$message=null 
	) {
		// Call parent constructor
		parent::__construct($id, $required, $type, $label, $placeholder, $conditions, $options,$message );
	}
	/**
	 * The propertyID for the image being uploaded
	 * @var Integer
	 */
	protected $propertyID;
	/**
	 * The landlord's userID for property selection.
	 * @var Integer
	 */
	protected $userID;
	/**
	 * The landlord's userID for property selection (duplicate).
	 * @var Integer
	 */
	protected $landlordID;
	/**
	 * Set to the existing pictureID if an existing image is being edited.
	 * @var Integer
	 */
	protected $pictureID;
	/**
	 * Returns the pictureID if its set
	 */
	protected function set_pictureID() {
		if (isset($_GET["pictureID"])) 
			$this->pictureID = $_GET["pictureID"];
	}
	/**
	 * Returns the propertyID if its set
	 */
	protected function set_propertyID() {
		if (isset($_GET["propertyID"])) 
			$this->propertyID = $_GET["propertyID"];
	}
	/**
	 * This method sets the form's userID from the $_SESSION data. 
	 */
	public function set_userID() {
		$this->userID = $_SESSION["userID"];
	}

	/**
	 * Sets the landlordID for this picture if it exists.
	 */
	public function set_landlordID() {
		// Set the landlordID if it exists
		$this->landlordID = $_SESSION["userID"];
	}
}
?>
