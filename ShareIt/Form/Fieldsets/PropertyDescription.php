<?php
/**
 * This file contains the PropertyDescription Fieldset class.
 */
namespace ShareIt\Form\Fieldsets;
use ShareIt\Form\FieldSet;
use TeamRad\Helpers\Opt as Opt;
use TeamRad\Helpers\Cnd as Cnd;
// Field Classes for this fieldset
use ShareIt\Form\Fieldsets\Field as Field;
use ShareIt\Form\Fieldsets\Field\Comments as Comments;
use ShareIt\Form\Fieldsets\Field\CostPerDay as CostPerDay;
/**
 * This class extends Fieldset. It defines a fieldset for a property
 * description on the ShareIT website.
 */
class PropertyDescription extends \ShareIt\Form\FieldSet {
	/**
	 * Sets the fieldset properties and adds fields.
	 */
	public function __construct() {
		$this->set_label(null, "Description");
		$this->add_field(new Comments());
		$this->add_field(new CostPerDay());
	} // end __construct()
} // end class 	
?>