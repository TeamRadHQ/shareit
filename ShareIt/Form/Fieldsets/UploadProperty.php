<?php
/**
 * This file contains the UploadProperty Fieldset class.
 */
namespace ShareIt\Form\Fieldsets;
// Helpers
use TeamRad\Helpers\Opt as Opt;
use TeamRad\Helpers\Cnd as Cnd;
// Field Classes for this fieldset
use ShareIt\Form\Fieldsets\Field as Field;
use ShareIt\Form\Fieldsets\Field\Property as Property;
/**
 * The UploadProperty Fieldset class defines a selector 
 * for choosing a property. This is used by the upload 
 * form to allow a landlord to select one of the 
 * properties and add images to it. 
 */
class UploadProperty extends \ShareIt\Form\FieldSet {
	/**
	 * Sets the fieldset properties, then gets the landlord's 
	 * existing properties from the database. 
	 */
	public function __construct() {
		// Set the fieldset label
		$this->set_label(null, "Property");
		// Add the fields to the fieldset
		$this->add_field(new Property("propertyID", true, ''));
	} // end __construct()
} // end class 	
?>