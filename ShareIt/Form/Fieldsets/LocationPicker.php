<?php
/**
 * This file containst the LocationPicker Fieldset Class
 */
namespace ShareIt\Form\Fieldsets;use TeamRad\Helpers\Opt as Opt;
use TeamRad\Helpers\Cnd as Cnd;
// Field Classes for this fieldset
use ShareIt\Form\Fieldsets\Field\Country as Country;
use ShareIt\Form\Fieldsets\Field\Location as Location;
/**
 * This class extends Fieldset. It defines a fieldset for 
 * selecting a location or country on the ShareIt website.
 */
class LocationPicker extends \ShareIt\Form\FieldSet {
	/**
	 * Sets the fieldset properties and adds fields.
	 */
	public function __construct() {
		$this->set_label(null, "Location");
		$this->add_field(new Country());
		$this->add_field(new Location());
	} // end __construct()
} // end class 	
?>