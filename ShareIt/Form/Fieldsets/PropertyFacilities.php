<?php
/**
 * This file contains the PropertyFacilities Fieldset class.
 */
namespace ShareIt\Form\Fieldsets;
use TeamRad\Helpers\Opt as Opt;
use TeamRad\Helpers\Cnd as Cnd;
// Field Classes for this fieldset
use ShareIt\Form\Fieldsets\Field as Field;
use ShareIt\Form\Fieldsets\Field\Kitchen as Kitchen;
use ShareIt\Form\Fieldsets\Field\Laundry as Laundry;
use ShareIt\Form\Fieldsets\Field\Toilet as Toilet;
/**
 * This class extends Fieldset. It defines a fieldset for information
 * about a property's facilities on the ShareIT website.
 */
class PropertyFacilities extends \ShareIt\Form\FieldSet {
	/**
	 * Sets the fieldset properties and adds fields.
	 */
	public function __construct() {
		$this->set_label(null, "Facilities");
		// Add the fields
		$this->add_field(new Kitchen());
		$this->add_field(new Laundry());
		$this->add_field(new Toilet());
	} // end __construct()
} // end class 	
?>