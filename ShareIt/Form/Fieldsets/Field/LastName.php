<?php
/**
 * This file contains the LastName Field class.
 */
namespace ShareIt\Form\Fieldsets\Field;
use TeamRad\Helpers\Opt as Opt;
use TeamRad\Helpers\Cnd as Cnd;
/**
 * Use this to create a last name field object.
 */
class LastName extends \ShareIt\Form\Field {
	/**
	 * Defines the LastName Field properties.
	 */
	public function __construct() {
		// Call the parent constructor.
		parent::__construct('lastname');
		$this->set_label('Last Name');
		$this->set_type('text_spaces');
		$this->set_placeholder('Your last name...');
		$this->cnd_maxlen(50);
	} // end __construct()
} // end class 	
?>