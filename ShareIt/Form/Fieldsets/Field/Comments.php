<?php
/**
 * This file contains the Comments Field class.
 */
namespace ShareIt\Form\Fieldsets\Field;
use TeamRad\Helpers\Opt as Opt;
use TeamRad\Helpers\Cnd as Cnd;
/**
 * The Comments Field class is used to add a field
 * form comments about a property or image on the 
 * ShareIt website.
 */
class Comments extends \ShareIt\Form\Field {
	/**
	 * Sets the field properties and calls parent constructor.
	 */
	public function __construct() {
		// Call the parent constructor.
		parent::__construct("comments", true, "textarea", "Comment", "Add a comment about this property...", Cnd::maxlen(100));
	} // end __construct()
} // end class 	
?>