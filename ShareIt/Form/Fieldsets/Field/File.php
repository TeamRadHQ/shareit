<?php
/**
 * This file contains the File Field class.
 */
namespace ShareIt\Form\Fieldsets\Field;
use TeamRad\Helpers\Opt as Opt;
use TeamRad\Helpers\Cnd as Cnd;
/**
 * Use this to create a File field which will validate
 * and upload a file to the ShareIt website.
 * @todo Fix issues with setting file name for upload.
 * @todo Add error handling for failed uploads to prevent data insertion.
 */
class File extends \ShareIt\Form\Field {
	/**
	 * The number of images already uploaded for this image.
	 * @var integer
	 */
	protected $count;
	/**
	 * An array of allowed file extensions for this file.
	 * @var Array
	 */
	protected $allowed_types = ["png", "jpg", "jpeg"];
	/**
	 * An array for storing the $_FILES data for the uploaded file.
	 * @var array
	 */
	protected $file = array();
	/**
	 * The maximum allowed file size in bytes.
	 * @var Integer
	 */
	protected $max_size;
	/**
	 * The upload target directory.
	 * @var String
	 */
	protected $target_dir = '/uploads/';
	/**
	 * A preformatted file name for the file's target location.
	 * @var String
	 */
	protected $file_name;
	/**
	 * Constructs a new File object. 
	 * An Id must be passed at construction. However, all other 
	 * arguments are optional. 
	 * @param string  $id          (Required) A unique identifier for this field
	 */

	public function __construct() {
		parent::__construct("file", false, 'file', 'Upload File', 'Choose a file to upload');
		// Run the file object setter
		$this->setter();
		$this->checker();
		$this->upload();
		// $this->debug();
	}
	/**
	 * Runs all of the set methods for this fields.
	 */
	protected function setter() {
		// Set the form variables.
		$this->set_landlordID();
		$this->set_pictureID();
		$this->set_propertyID();
		$this->set_max_size(204800*1.25);		
		$this->set_count();
		// Set the file, its name and upload location.
		$this->set_file();
		$this->set_target_dir();
		$this->set_file_name();
	}
	/**
	 * Runs all of the validation checks for the
	 * file upload.
	 */
	protected function checker() {
		// Stop if there is no POST data.
		if(!count($_POST)) return;
		// Otherwise, check the size.
		if($this->check_size())
			// If size is okay, then check the type.
			$this->check_type();
	}
	/**
	 * Sets the propertyID for this picture.
	 */
	public function set_count(){
		// Get the image count from the database
		if($this->propertyID)
			// Call the ShareIt img_count function
			$this->count = \ShareIt\img_count($this->propertyID);
		
	}
	/**
	 * Sets a value for this field for parent class functionality. Eventually, 
	 * this will hold the value of the database or uploaded file name for use 
	 * in Mustache templates.
	 * @param string $value 	The value to be set.
	 */
	public function set_value($value="") {
		$this->value = $value;
	}
	/**
	 * Gets and sets the $_FILES array for this file if it exists.
	 */
	public function set_file() {
		// If there is a file with this field's id.
		if( isset($_FILES[$this->id()]) ) {
			$this->file = $_FILES[$this->id()];
			// Now clear the array if the file is empy
			if ($this->file["name"] == "") $this->file = array();
		}
	}
	/**
	 * Gets and sets the $_FILES array for this file if it exists.
	 */
	public function set_target_dir() {
		// If there is a file with this field's id.
		$this->target_dir = $_SERVER["DOCUMENT_ROOT"] . $this->target_dir;
	}
	/**
	 * Sets the maximum allowed file size in bytes.
	 * @param Integer $size The maximum file size in bytes.
	 */
	public function set_max_size($size) {
		$this->max_size = $size;
	}
	/**
	 * Sets a new file name or gets an existing file name for this file upload. 
	 * @todo  Need to simplify this process. No problems with naming new files. 
	 * However, there are sporadic errors with naming uploads that replace existing
	 * file names.  
	 */
	public function set_file_name() {
		// Set a new file name if there is no pictureID
		if (!$this->pictureID) {
			$name = $this->new_file_name();
		} else {
			// Otherwise, get the existing file name from the database.
			$name = $this->db_file_name($this->pictureID); 
		}
		// Set the file name to value so it can be passed up the form
		$this->file_name = $name;
		if ($this->extension($name)) {
			$this->set_value($this->file_name);
		}
	}
	/**
	 * Creates the file name for a new upload. 
	 * @todo  Remove the check for uploaded file and create a method
	 * for handling this. 
	 * @return string The file name stored in the database. 
	 */
	private function new_file_name() {
		$num = $this->count + 1;
		$name = $this->landlordID . 'p' . $this->propertyID . '-img-' . $num ;
		if($this->file)
			$name = $name . "." . $this->extension();
		return $name;
	}
	/**
	 * Gets the file name of an existing file from the database.
	 * @return string The file name that is stored in the database. 
	 */
	private function db_file_name() {
		// Get the file name from the database
		$db_name = \ShareIt\img_name($this->pictureID);
		// Extract the file extension
		$db_ext = $this->extension($db_name);
		// Return the file name
		return $db_name;
	}

	/**
	 * gets the pictureID for this picture if it exists.
	 */
	public function get_pictureID() {
		return $this->pictureID;
	}
	/**
	 * gets the propertyID for this picture.
	 */
	public function get_propertyID(){
		return $this->propertyID;
	}
	/**
	 * Gets and gets the $_FILES array for this file if it exists.
	 */
	public function get_file() {
		return $this->file;
	}
	/**
	 * gets the maximum allowed size for an uploaded file. 
	 */
	public function get_max_size() {
		return $this->max_size;
	}
	/**
	 * gets a new file name or gets an existing file name for this file upload. 
	 */
	public function get_file_name() {
		return $this->file_name;
	}
	/**
	 * Sets a value for this field for parent class functionality.
	 * @return String The file name.
	 */
	public function get_value() {
		return $this->value;
	}
	/**
	 * This method names and moves and uploaded file to the uploads directory.
	 * @todo  add verification check for successful upload.
	 * @return boolean 	Returns true if the file submitted was uploaded, or there was no submitted file.
	 *                          false if the file submitted has errors.
	 */
	protected function upload() {
		// Stop and return true if there's no file name
		// (because it means there is no upload so it passes)
		if ( !count($this->file) )
			return;
		// Stop and return false if there are errors 
		// (becuase there are errors the upload failed)
		if( $this->error_count() )
			return;
		// Otherwise there are no errors so prepare the upload. 
		// Format the file name and upload location
		$target_file = $this->target_dir . $this->file_name;
		// Stop if there's no ext
		// Unlink the file if it already exists.
		if(file_exists($target_file)) unlink($target_file);
		// Move the uploaded file.
		move_uploaded_file($this->file["tmp_name"], $target_file);
		// The file was successfully uploaded.
		return true;
	}
	/**
	 * Checks to see if an uploaded file type is in the allowed types array
	 * and if not, it adds an error.
	 * @return Boolean	 Returns true if the file type is an allowed type.
	 *                           false if the file type is not an allowed type.
	 */
	protected function check_type() {
		// Return if there's no file.
		if (!$this->extension())
			return;
		// Check if the file extension is in the allowed types array.
		if (in_array($this->extension(), $this->allowed_types)) {
			return true;
		} else {
			// Otherwise, add an error which includes the allowed types.
			$this->add_error("You have uploaded an invalid file type. Allowed types: ".implode(', ', $this->allowed_types).".");
			return false;
		}
	}
	/**
	 * If a file is uploaded its size is checked. 
	 * @return Boolean Returns true if there is no file or the file is within the allowed file size.
	 *                         false if there is a file, but its file size is outside of the contstraints.
	 */
	protected function check_size() {
		// If there is no file, there is no error. So things are coolio!
		if( !$this->file ) return true;
		// Otherwise, if the file has no size then stop, because there's an error.
		if( !$this->zero_size()) return false;
		// Failing that, if the file is too large then stop, because that's bad.
		if( !$this->max_size() ) return false;
		// Otherwise, if you've made it this far, it's only logical to assume everything is awesome!
		return true;
	}
	/**
	 * Checks to see if an uploaded file has a size. If there is a file and
	 * it has no size, an error is added.
	 * @return Boolean 	Returns true if there is a file and it has a size, 
	 *                          false if there isn't a file and it's a new upload
	 */
	protected function zero_size() {
		// Add an error if there is no pictureID or file
		if (!$this->file["size"] && !$this->pictureID) {
				$this->add_error("You haven't uploaded an image.");
				return false;
		} else {
			return true; // The file has a size.
		}
	} // end zero_size()
	/**
	 * Checks to see if an uploaded file's size is lower than the maximum.
	 * If it is too large, an error is added.
	 * @return Boolean Returns true if the file size is less than the maximum allowed.
	 *                         false if the file size is more than the maximum allowed.
	 */
	protected function max_size() {
		// Add an error if the file size exceeds the maximum allowed.
		if ($this->file["size"] > $this->max_size) {
			// Format the size and max_size
			$size = $this->format_bytes($this->file["size"]);
			$max_size = $this->format_bytes($this->max_size);
			// Add an error
			$this->add_error("This file is ".$size.". It should be less than ".$max_size.".");
			return false;
		} else {
			// Otherwise the file is less than the maximum allowed.
			return true;
		}
	} // end max_size()
	/**
	 * Returns the file extension of an uploaded file.
	 * @param  String $file_name A file name.
	 * @return String The uploaded file's extension.
	 */
	protected function extension($file_name = null) {
		$extension = "";
		if (!$file_name && $this->file) $file_name = $this->file["name"];
		$extension = pathinfo($file_name, PATHINFO_EXTENSION);
		return $extension;
	}
	/**
	 * Formats the byte file sizes into human readable kilobytes.
	 * @param  integer  $bytes     The file size in bytes.
	 * @param  integer $precision The number of decimals in conversion?
	 * @return String             A formatted file size with unit measurements.
	 */
	protected function format_bytes($bytes, $precision = 2) { 
	    $units = array('B', 'KB', 'MB', 'GB', 'TB'); 
	    $bytes = max($bytes, 0); 
	    $pow = floor(($bytes ? log($bytes) : 0) / log(1024)); 
	    $pow = min($pow, count($units) - 1); 

	    // Uncomment one of the following alternatives
	    $bytes /= pow(1024, $pow);
	    // $bytes /= (1 << (10 * $pow)); 

	    return round($bytes, $precision) . ' ' . $units[$pow]; 
	} 
	/**
	 * Outputs object properties for debugging
	 * @return HTML The object properties.
	 */
	private function debug() {
		echo '<pre class="container">';
		echo '<table class="table table-responsive">';
		echo '<thead><th class="col-sm-6" colspan="2" ><h1>File Debug</h1></th></thead>';
		echo '<tr><td class="col-sm-3"><b> landlordID:</b></td><td>' . $this->landlordID . '</td></tr>';
		echo '<tr><td class="col-sm-3"><b> PictureID:</b></td><td>' . $this->pictureID . '</td></tr>';
		echo '<tr><td class="col-sm-3"><b> propertyID:</b></td><td>' . $this->propertyID . '</td></tr>';
		echo '<tr><td class="col-sm-3"><b> value:</b></td><td>' . $this->value . '</td></tr>';
		echo '<tr><td class="col-sm-3"><b> img_count:</b></td><td>' . $this->count . '</td></tr>';
		echo '<tr><td class="col-sm-3"><b> file:</b></td><td><pre>', print_r($this->file), '</pre></td></tr>';
		echo '<tr><td class="col-sm-3"><b> extension:</b></td><td>', $this->extension(),  '</td></tr>';
		echo '<tr><td class="col-sm-3"><b> max_size:</b></td><td>' . $this->max_size . '</td></tr>';
		echo '<tr><td class="col-sm-3"><b> file_name:</b></td><td>' . $this->file_name . '</td></tr>';
		echo '<tr><td class="col-sm-3"><b> target_dir:</b></td><td>' . $this->target_dir . '</td></tr>';
		echo '<tr><td class="col-sm-3"><b> error_count:</b></td><td> ' . $this->error_count() . '</td></tr>';
		echo '<tr><td class="col-sm-3"><b> errors:</b></td><td> <pre>', print_r($this->errors),	'</pre></td></tr>';
		echo "</table>";
		echo "</pre>";

	}

} // End class
?>