<?php
/**
 * This file contains the Country Field class. 
 */
namespace ShareIt\Form\Fieldsets\Field;
use TeamRad\Helpers\Opt as Opt;
use TeamRad\Helpers\Cnd as Cnd;
/**
 * The Country Field object allows a landlord to select
 * a country. It will be used in property listings, landlord 
 * profiles and user searches.  
 */
class Country extends \ShareIt\Form\Field {
	/**
	 * Calls the parent constructor and defines the field properties.
	 * Then queries the ShareIt database to get countries and add them
	 * as options. 
	 */
	public function __construct() {
		// Call the parent constructor.
		parent::__construct('countrycode');
		// Set the field properties
		$this->set_label('Country');
		$this->set_placeholder('Select a country');
		$this->set_type('select');
		// Add the location option value pairs from the database
		$this->set_options(Opt::db_values("SELECT countryCode, CommonName FROM country ORDER BY CommonName"));

	} // end __construct()
} // end class 	
?>