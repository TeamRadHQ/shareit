<?php
/**
 * This file contains the CostPerDay Field class.
 */
namespace ShareIt\Form\Fieldsets\Field;
use TeamRad\Helpers\Opt as Opt;
use TeamRad\Helpers\Cnd as Cnd;
/**
 * This field is used to enter the daily cost of a property
 * on the ShareIt website. 
 */
class CostPerDay extends \ShareIt\Form\Field {
	/**
	 * Sets the field properties.
	 */
	public function __construct() {
		// Add address field
		// Call the parent constructor.
		parent::__construct("costperday", true, "float", "Daily Rate", "Add the daily rate...", Cnd::maxlen(10));
		$this->cnd_pos();
	} // end __construct()
} // end class 	
?>