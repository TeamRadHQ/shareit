<?php
/**
 * This file contains the BedDouble Field class.
 */
namespace ShareIt\Form\Fieldsets\Field;
use TeamRad\Helpers\Opt as Opt;
use TeamRad\Helpers\Cnd as Cnd;
/**
 * BedDouble defines a field which allows a landlord to
 * store the number of double beds a property contains. 
 */
class BedDouble extends \ShareIt\Form\Field {
	/**
	 * Calls the parent constructor then sets the 
	 * field properties. 
	 */
	public function __construct() {
		// Call the parent constructor.
		parent::__construct('beddouble');
		$this->set_id('beddouble');
		$this->set_label('Double Beds');
		$this->set_placeholder('Double Beds');
		$this->set_required(false);
		$this->set_type('select');
		$this->set_options(Opt::numbers(1,10));
	} // end __construct()
} // end class 	
?>