<?php
/**
 * This file contains the Bedrooms Field class. 
 */
namespace ShareIt\Form\Fieldsets\Field;
use TeamRad\Helpers\Opt as Opt;
use TeamRad\Helpers\Cnd as Cnd;
/**
 * The Bedrooms Field class defines a field for 
 * adding a description of a property's bedrooms. 
 */
class Bedrooms extends \ShareIt\Form\Field {
	/**
	 * Sets the field properties. 
	 */
	public function __construct() {
		// Call the parent constructor.
		parent::__construct('bedrooms');
		// Set the field properties
		$this->set_label('Description');
		$this->set_type('textarea');
		$this->set_placeholder('Description of bedrooms...');
		$this->cnd_maxlen(100);
	} // end __construct()
} // end class 	
?>