<?php
/**
 * This file contains the Email Field class.
 */
namespace ShareIt\Form\Fieldsets\Field;
use TeamRad\Helpers\Opt as Opt;
use TeamRad\Helpers\Cnd as Cnd;
/**
 * Use this to create an email field for adding emails
 * to the ShareIt database.
 */
class Email extends \ShareIt\Form\Field {
	/**
	 * Sets the field properties.
	 */
	public function __construct() {
		// Add address field
		// Call the parent constructor.
		parent::__construct('email');
		$this->set_type('email');
		$this->set_label('Email');
		$this->set_placeholder('Your email...');
		$this->cnd_maxlen(25);
	} // end __construct()
} // end class 	
?>