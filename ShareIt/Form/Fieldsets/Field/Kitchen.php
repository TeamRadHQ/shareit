<?php
/**
 * This file contains the Kitchen Field class.
 */
namespace ShareIt\Form\Fieldsets\Field;
use TeamRad\Helpers\Opt as Opt;
use TeamRad\Helpers\Cnd as Cnd;
/**
 * Use this to create a field object for address line 1
 */
class Kitchen extends \ShareIt\Form\Field {
	/**
	 * Sets the fieldset properties and adds fields.
	 */
	public function __construct() {
		// Add address field
		// Call the parent constructor.
		parent::__construct("kitchen", true, "textarea", "Kitchen", "Description of kitchen...", Cnd::maxlen(100));
	} // end __construct()
} // end class 	
?>