<?php
/**
 * This file defines a field for picking properties.
 */
namespace ShareIt\Form\Fieldsets\Field;
use TeamRad\Helpers\Opt as Opt;
use TeamRad\Helpers\Cnd as Cnd;
/**
 * This field can be used to generate a property select field
 * for a logged in user. 
 */
class Property extends \ShareIt\Form\Field {
	/**
	 * Runs the parent constructor, then sets field properties. 
	 * Gets the 
	 * @param string $prefix [description]
	 * @param string $suffix [description]
	 */
	public function __construct($prefix="", $suffix="") {
		// Call the parent constructor.
		parent::__construct('propertyID');
		// Set the field properties
		$this->set_label('Property');
		$this->set_placeholder('Select a property');
		$this->set_type('select');
		// Add the location option value pairs from the database
		$this->set_options(Opt::db_values("SELECT propertyID, CONCAT(Street, ', ',Suburb) 
			FROM property p, location l
			WHERE p.locationID = l.locationID
			ORDER BY Street
			"));
		// Set the property if propertyID has been passed
		if(isset($_GET["propertyID"]))
			$this->set_value($_GET["propertyID"]);
	} // end __construct()
} // end class 	
?>