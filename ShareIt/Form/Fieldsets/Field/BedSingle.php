<?php
/**
 * This file contains the BedSingle Field class.
 */
namespace ShareIt\Form\Fieldsets\Field;
use TeamRad\Helpers\Opt as Opt;
use TeamRad\Helpers\Cnd as Cnd;
/**
 * Use this to create a field object for address line 1
 */
class BedSingle extends \ShareIt\Form\Field {
	/**
	 * Calls the parent constructor then sets the 
	 * field properties. 
	 */
	public function __construct() {
		// Add address field
		// Call the parent constructor.
		parent::__construct('bedsingle');
		$this->set_id('bedsingle');
		$this->set_label('Single Beds');
		$this->set_placeholder('Single Beds');
		$this->set_required(false);
		$this->set_type('select');
		$this->set_options(Opt::numbers(1,10));
	} // end __construct()
} // end class 	
?>