<?php
/**
 * This file contains the Location Field class.
 */
namespace ShareIt\Form\Fieldsets\Field;
use TeamRad\Helpers\Opt as Opt;
use TeamRad\Helpers\Cnd as Cnd;
/**
 * Use this to create a field object for selecting
 * a location from the ShareIt database. 
 */
class Location extends \ShareIt\Form\Field {
	/**
	 * Calls the parent constructor and adds field properties.
	 */
	public function __construct() {
		// Call the parent constructor.
		parent::__construct('locationID');
		// Set the field properties
		$this->set_label('Location');
		$this->set_placeholder('Property Location');
		$this->set_type('select');
		// Add the location option value pairs from the database
		$this->set_options(Opt::db_values("SELECT locationID, locationname FROM location ORDER BY locationname"));
	} // end __construct()
} // end class 	
?>