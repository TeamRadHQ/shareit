<?php
/**
 * This file contains the Laundry Field class. 
 */
namespace ShareIt\Form\Fieldsets\Field;
use TeamRad\Helpers\Opt as Opt;
use TeamRad\Helpers\Cnd as Cnd;
/**
 * The Laundry Field object allows a landlord to enter 
 * information about a property's laundry facilities. 
 */
class Laundry extends \ShareIt\Form\Field {
	/**
	 * Calls the parent constructor and defines field properties. 
	 */
	public function __construct() {
		// Call the parent constructor.
		parent::__construct("laundry", true, "textarea", "Laundry", "Description of laundry...", Cnd::maxlen(100));
	} // end __construct()
} // end class 	
?>