<?php
/**
 * This file contains the Location Field class
 */
namespace ShareIt\Form\Fieldsets\Field;
use TeamRad\Helpers\Opt as Opt;
use TeamRad\Helpers\Cnd as Cnd;
/**
 * This field allows landlords to add a property's 
 * postcode to a property listing.
 */
class Postcode extends \ShareIt\Form\Field {
	/**
	 * Calls the parent constructor and then
	 * sets the field properties.
	 */
	public function __construct() {
		// Call the parent constructor
		parent::__construct('postcode');
		// Set the field properties
		$this->set_label('Postcode');
		$this->set_placeholder('Property postcode...');
		$this->cnd_maxlen(10);
	} // end __construct()
} // end class 	
?>