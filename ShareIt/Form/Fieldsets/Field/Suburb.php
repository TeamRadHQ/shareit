<?php
/**
 * This file contains the Suburb Field class.
 */
namespace ShareIt\Form\Fieldsets\Field;
use TeamRad\Helpers\Opt as Opt;
use TeamRad\Helpers\Cnd as Cnd;
/**
 * This field allows the landlord to enter
 * a property's suburb.
 */
class Suburb extends \ShareIt\Form\Field {
	/**
	 * Defines the Street Field properties.
	 */
	public function __construct() {
		// Call the parent constructor.
		parent::__construct('suburb');
		// Set the field properties
		$this->set_label('Suburb');
		$this->set_placeholder('Your suburb...');
		$this->cnd_maxlen(30);
	} // end __construct()
} // end class 	
?>