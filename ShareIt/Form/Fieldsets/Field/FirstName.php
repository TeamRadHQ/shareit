<?php
/**
 * This file contains the FirstName Field class.
 */
namespace ShareIt\Form\Fieldsets\Field;
use TeamRad\Helpers\Opt as Opt;
use TeamRad\Helpers\Cnd as Cnd;
/**
 * Use this to create a first name field object.
 */
class FirstName extends \ShareIt\Form\Field {
	/**
	 * Defines the FirstName Field properties.
	 */
	public function __construct() {
		// Call the parent constructor.
		parent::__construct('firstname');
		$this->set_label('First Name');
		$this->set_type('text_spaces');
		$this->set_placeholder('Your first name...');
		$this->cnd_maxlen(50);
	} // end __construct()
} // end class 	
?>