<?php
/**
 * This file contains the Street Field class.
 */
namespace ShareIt\Form\Fieldsets\Field;
use TeamRad\Helpers\Opt as Opt;
use TeamRad\Helpers\Cnd as Cnd;
/**
 * This field allows the landlord to enter
 * a property's address.
 */
class Street extends \ShareIt\Form\Field {
	/**
	 * Defines the Street Field properties.
	 */
	public function __construct() {
		// Call the parent constructor.
		parent::__construct('street');
		// Set the field properties
		$this->set_label('Street');
		$this->set_placeholder('Street address...');
		$this->cnd_maxlen(25);
	} // end __construct()
} // end class 	
?>