<?php
/**
 * This file contains the FeaturedImage class.
 */
namespace ShareIt\Form\Fieldsets\Field;
use TeamRad\Helpers\Opt as Opt;
use TeamRad\Helpers\Cnd as Cnd;
/**
 * FeaturedImage defines a field to allow landlords
 * to set a featured image for a property.
 */
class FeaturedImage extends \ShareIt\Form\Field {
	/**
	 * Sets the field properties and adds options.
	 */
	public function __construct() {
		// Call the parent constructor.
		parent::__construct("featuredImage", false, "radio", "Featured Image");
		// Remove the label
		$this->set_label(' ');
		$this->set_required(false);
		// Add the checkbox
		$this->set_option(array("value" => 0, "option" => "No"));
		$this->set_option(array("value" => 1, "option" => "Yes"));
		$this->set_POST_value();
	} // end __construct()
} // end class 	
?>