<?php
/**
 * This file defines a field for phone numbers. 
 * @uses ShareIt\Form\Field
 * @uses TeamRad\Helpers\Opt as Opt
 * @uses TeamRad\Helpers\Cnd as Cnd 
 */
namespace ShareIt\Form\Fieldsets\Field;
use TeamRad\Helpers\Opt as Opt;
use TeamRad\Helpers\Cnd as Cnd;
/**
 * This class defines a field which is used in the 
 * ShareIt website for collecting MOB1 field data for 
 * the landlord table. 
 */
class Mob2 extends \ShareIt\Form\Field {
	/**
	 * Constructs a Field object and sets properties.
	 */
	public function __construct() {
		// Add address field
		// Call the parent constructor.
		parent::__construct("mob2");
		$this->set_label('Alternate Phone');
		$this->set_placeholder('Your alternate contact...');
		$this->cnd_maxlen(25);
	} // end __construct()
} // end class 	
?>