<?php
/**
 * This file contains the Toilet Field class.
 */
namespace ShareIt\Form\Fieldsets\Field;



use TeamRad\Helpers\Opt as Opt;
use TeamRad\Helpers\Cnd as Cnd;
/**
 * The Toilet class defines a field to allow a landlord
 * to add a description of a property's bathroom and toilets.
 */
class Toilet extends \ShareIt\Form\Field {
	/**
	 * Calls the parent constructor and sets field properties.
	 */
	public function __construct() {
		// Call the parent constructor.
		parent::__construct("toilet", true, "textarea", "Toilet", "Description of toilet...", Cnd::maxlen(100));
	} // end __construct()
} // end class 	
?>