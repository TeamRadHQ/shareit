<?php
/**
 * This file contains the ProfilePassword Fieldset class.
 */
namespace ShareIt\Form\Fieldsets;

use TeamRad\Helpers\Opt as Opt;
use TeamRad\Helpers\Cnd as Cnd;
// Field Classes for this fieldset
use ShareIt\Form\Fieldsets\Field as Field;
// use ShareIt\Form\Fieldsets\Field\PasswordNew as PasswordNew;
use ShareIt\Form\Fieldsets\Field\Email as Email;
/**
 * This ProfileName Fieldset class defines a fieldset to
 * add a landlord's name to their profile.
 */
class ProfilePassword extends \ShareIt\Form\FieldSet {
	/**
	 * Sets the fieldset properties and adds fields. 
	 */
	public function __construct() {
		$this->set_label(null, "Password");
		$username = new \ShareIt\Form\Field("username");
		$username->set_type('text');
		$password = new \ShareIt\Form\Field("password");
		$password->set_type('passwordNew');
		$this->add_field(new Email() );
		$this->add_field($username);
		$this->add_field($password);
	} // end __construct()
} // end class 	
?>