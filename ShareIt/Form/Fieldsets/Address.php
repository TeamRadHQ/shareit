<?php
/**
 * This file contains the Address Fieldset class.
 */
namespace ShareIt\Form\Fieldsets;
use TeamRad\Helpers\Opt as Opt;
use TeamRad\Helpers\Cnd as Cnd;
// Field classes
use ShareIt\Form\Fieldsets\Field as Field;
use ShareIt\Form\Fieldsets\Field\Street as Street;
use ShareIt\Form\Fieldsets\Field\Suburb as Suburb;
use ShareIt\Form\Fieldsets\Field\Postcode as Postcode;
use ShareIt\Form\Fieldsets\Field\Location as Location;
use ShareIt\Form\Fieldsets\Field\Country as Country;
/**
 * This class extends Fieldset. It defines a fieldset for a property's
 * location on the ShareIt website.
 */
class Address extends \ShareIt\Form\FieldSet {
	/**
	 * Sets the fieldset properties and adds fields.
	 */
	public function __construct() {
		// Add the fields
		$this->set_label(null, "Address");
		$this->add_field(new Street());
		$this->add_field(new Suburb());
		$this->add_field(new Postcode());
	} // end __construct()
} // end class 	
?>