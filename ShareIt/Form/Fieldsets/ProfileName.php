<?php
/**
 * This file contains the ProfileName Fieldset class.
 */
namespace ShareIt\Form\Fieldsets;
use TeamRad\Helpers\Opt as Opt;
use TeamRad\Helpers\Cnd as Cnd;
// Field Classes for this fieldset
use ShareIt\Form\Fieldsets\Field as Field;
use ShareIt\Form\Fieldsets\Field\FirstName as FirstName;
use ShareIt\Form\Fieldsets\Field\LastName as LastName;
/**
 * This ProfileName Fieldset class defines a fieldset to
 * add a landlord's name to their profile.
 */
class ProfileName extends \ShareIt\Form\FieldSet {
	/**
	 * Sets the fieldset properties and adds fields. 
	 */
	public function __construct() {
		$this->set_label(null, "Name");
		$this->add_field(new FirstName());
		$this->add_field(new LastName());
	} // end __construct()
} // end class 	
?>