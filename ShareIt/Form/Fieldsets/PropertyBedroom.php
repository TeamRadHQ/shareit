<?php
/**
 * This file contains the PropertyBedroom Fieldset class.
 */
namespace ShareIt\Form\Fieldsets;

// Helpers
use TeamRad\Helpers\Opt as Opt;
use TeamRad\Helpers\Cnd as Cnd;
// Field Classes for this fieldset
use ShareIt\Form\Fieldsets\Field as Field;
use ShareIt\Form\Fieldsets\Field\Bedrooms as Bedrooms;
use ShareIt\Form\Fieldsets\Field\BedSingle as BedSingle;
use ShareIt\Form\Fieldsets\Field\BedDouble as BedDouble;
/**
 * This class extends Fieldset. It defines a fieldset for information
 * about a property's bedrooms on the ShareIT website.
 */
class PropertyBedroom extends \ShareIt\Form\FieldSet {
	/**
	 * Sets the fieldset properties and adds fields.
	 */
	public function __construct() {
		// Set the fieldset label
		$this->set_label(null, "Bedrooms");
		// Add the fields to the fieldset
		$this->add_field(new Bedrooms());
		$this->add_field(new BedSingle());
		$this->add_field(new BedDouble());
	} // end __construct()
} // end class 	
?>