<?php
/**
 * This file contains the UploadFile Fieldset class.
 */
namespace ShareIt\Form\Fieldsets;

// Helpers
use TeamRad\Helpers\Opt as Opt;
use TeamRad\Helpers\Cnd as Cnd;
// Field Classes for this fieldset
use ShareIt\Form\Fieldsets\Field as Field;
use ShareIt\Form\Fieldsets\Field\File as File;
use ShareIt\Form\Fieldsets\Field\FeaturedImage as FeaturedImage;
use ShareIt\Form\Fieldsets\Field\Comments as Comments;
/**
 * This class extends Fieldset. It defines a fieldset for information
 * about a property's bedrooms on the ShareIT website.
 */
class UploadFile extends \ShareIt\Form\FieldSet {
	/**
	 * The upload name for this file.
	 * @var string
	 */
	protected $file_name;
	/**
	 * Sets the fieldset properties and adds fields.
	 */
	public function __construct() {
		// Set the fieldset label
		$this->set_label(null, "Upload an Image");
		// Add the fields to the fieldset
		$this->add_field(new File());
		$this->add_field(new Comments());
		$this->add_field(new FeaturedImage());
		$this->setter();
	} // end __construct()
	/**
	 * Runs all of the setter functions after object construction.
	 */
	public function setter() {
		$this->set_pictureID();
		$this->set_propertyID();
		$this->set_file_name();
	}
	/**
	 * Sets the file name as determined by the file field.
	 */
	public function set_file_name() {
		$file = $this->fieldset["file"];
		$this->file_name = $file->get_file_name();
	}
	/**
	 * Gets the file name that's been set.
	 * @return String The file name.
	 */
	public function get_file_name() {
		return $this->file_name();
	}
} // end class
?>