<?php
/**
 * This file defines a fieldset for a landlord's contact information. 
 * @uses ShareIt\Form\Fieldset
 * @uses ShareIt\Form\Fieldsets\Field\Email
 * @uses ShareIt\Form\Fieldsets\Field\Mob1
 * @uses ShareIt\Form\Fieldsets\Field\Mob2
 * @uses TeamRad\Helpers\Opt as Opt
 * @uses TeamRad\Helpers\Cnd as Cnd 
 */
namespace ShareIt\Form\Fieldsets;use TeamRad\Helpers\Opt as Opt;
use TeamRad\Helpers\Cnd as Cnd;
// Field Classes for this fieldset
use ShareIt\Form\Fieldsets\Field\Email as Email;
use ShareIt\Form\Fieldsets\Field\Mob1 as Mob1;
use ShareIt\Form\Fieldsets\Field\Mob2 as Mob2;
/**
 * This class extends Fieldset. It defines a fieldset for a landlord
 * to enter their contact information on the ShareIT website.
 */
class ProfileContact extends \ShareIt\Form\FieldSet {
	/**
	 * Constructs a Field object and sets properties.
	 */
	public function __construct() {
		$this->set_label(null, "Contact Information");
		$this->add_field(new Email());
		$this->add_field(new Mob1());
		$this->add_field(new Mob2());
	} // end __construct()
} // end class 	
?>