<?php
/**
 * This file contains the FormLogin Form class.
 */
namespace ShareIt\Form;
use \TeamRad\Helpers as Helpers;

/**
 * The FormLogin class extends the Form class. 
 * It generates a login form required to login to the website. 
 */
class FormLogin extends Form {
	/**
	 * The constructor adds an email and password field and sets the action to login.php.
	 */
	public function __construct() {
		// Create the fields
		$email = new \ShareIt\Form\Field("email");
		$email->set_type("email");
		$password = new \ShareIt\Form\Field("password");
		$password->set_type('password');

		$loginFields = new \TeamRad\Form\FieldSet("loginFields");
		$loginFields->set_label(null,"Login");
		$loginFields->add_field($email);
		$loginFields->add_field($password);

		$this->set_fieldset($loginFields);

		parent::__construct();
		$this->set_action('login.php');
		$this->set_method('POST');
	}
} // End class Form
