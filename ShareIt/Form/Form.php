<?php
/**
 * This file contains the ShareIt Form class.
 */
namespace ShareIt\Form;
use \TeamRad\Helpers as Helpers;
use \TeamRad\Form\Form as TRForm;
use \TeamRad\DB as DB;/**
 * This class extends the TeamRad form class. It
 * adds additional methods and properties used by the
 * ShareIt website.
 * @uses /TeamRad/Form/Form
 */
class Form extends TRForm {
	/**
	 * The constructor instantiates a new Mustache template object.
	 * @todo Add ability to pass options to constructor.
	 */
	public function __construct() {
		parent::__construct();
	} // end __construct()
	/**
	 * The propertyID for the image being uploaded
	 * @var Integer
	 */
	protected $propertyID;
	/**
	 * The landlord's userID for property selection.
	 * @var Integer
	 */
	protected $userID;
	/**
	 * The landlord's userID for property selection (duplicate).
	 * @var Integer
	 */
	protected $landlordID;
	/**
	 * Set to the existing pictureID if an existing image is being edited.
	 * @var Integer
	 */
	protected $pictureID;

	/**
	 * This method sets the form's userID from the $_SESSION data. 
	 */
	public function set_userID() {
		$this->userID = $_SESSION["userID"];
	}
	/**
	 * Sets the landlordID for this picture if it exists.
	 */
	public function set_landlordID() {
		// Set the landlordID if it exists
		$this->landlordID = $_SESSION["userID"];
	}

	/**
	 * Returns the pictureID if its set
	 */
	protected function set_pictureID() {
		if (isset($_GET["pictureID"])) 
			$this->pictureID = $_GET["pictureID"];
	}
	/**
	 * Returns the propertyID if its set
	 */
	protected function set_propertyID() {
		if (isset($_GET["propertyID"])) 
			$this->propertyID = $_GET["propertyID"];
	}
	/**
	 * Sets the form action and appends property and 
	 * picture id's if they're set.
	 * @param string $action The url for the form.
	 */
	public function set_action($action="") {
		$action .= '?';
		if($this->propertyID) {
			$action .= "propertyID=" . $this->propertyID . '&';
		}
		if($this->pictureID) {
			$action .= "pictureID=" . $this->pictureID;
		}
		parent::set_action($action);
	}

 } // End class Form
