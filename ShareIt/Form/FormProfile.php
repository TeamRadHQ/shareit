<?php
/**
 * This file contains the FormProfile class.
 */
namespace ShareIt\Form;

// Helpers
use \TeamRad\Helpers\Opt as Opt;
use \TeamRad\Helpers\Cnd as Cnd;
// Form FieldSets
// use \ShareIt\Form\Fieldsets\Profile as Profile;
use \ShareIt\Form\Fieldsets\ProfileName as ProfileName;
use \ShareIt\Form\Fieldsets\ProfileContact as ProfileContact;
use \ShareIt\Form\Fieldsets\LocationPicker as ProfileLocation;

/**
 * FormProfile extends the Form class.
 * It is used to create a form for a landlord to update their profile
 * on the ShareIT website. 
 */
class FormProfile extends Form {
	/**
	 * The constructor sets the userID from the website session. It then adds 
	 * fieldsets for the landlord's name, location and contact information.
	 * If the user hasn't submitted data, then it will collect this from the 
	 * ShareIT database and add it to the form. 
	 * If the user has submitted the form, the input data will be validated
	 * and the database will be updated if there are no errors. 
	 */
	public function __construct() {
		// Set the userID
		$this->set_userID();
		parent::__construct();
		$this->set_fieldset(new ProfileName());
		$this->set_fieldset(new ProfileLocation());
		$this->set_fieldset(new ProfileContact());
		// Get the property data from the database if there is no $_POST data		
		if (count($_POST) < 1) {
			$user = $this->get_data();
			if (is_array($user))
				$this->set_values($user);
		} // end if(count)

		// Set the form action
		$this->set_action("profile.php");
		// Set the post values
		$this->set_POST_values();

		// Update the profile if data has been submitted
		if (count($_POST) > 0 && $this->error_count < 1) {
			$this->update_data();
		}
	}
	/**
	 * This method binds the form data then executes a database update query.	
	 * @return Redirect Redirects the user upon update.
	 */
	protected function update_data() {
		$db = new DB;
		$values = $this->prep_db_values();
		// Prepare the query
		$query = $db->prepare($this->qry_update);
		// Execute
		$query->execute($values);
		header("Location: http://shareit.org/profile.php?update=1");
		die();
	}
	/**
	 * This function prepares the form field input values so that they can be 
	 * bound to the insert and update database queries.
	 * @return Array An array of bound database field values.
	 */
	protected function prep_db_values() {
		$values = $this->get_values();
		$values = [
			":email" 		=> $values["email"],
			":firstname" 		=> $values["firstname"],
			":lastname" 		=> $values["lastname"],
			":countrycode" 		=> $values["countrycode"],
			":locationID" 		=> $values["locationID"],
			":mob1" 		=> $values["mob1"],
			":mob2" 		=> $values["mob2"],
			":userID" 		=> $this->userID,
		];
		return $values;
	}
	/**
	 * This method prepares the default queries for this form. 
	 * If a propertyID exists, it sets SELECT and UPDATE queries.
	 * If a propertyID does not exist, it sets a SELECT and INSERT query. 
	 */
	protected function form_queries() {
		// Set select query if there is a propertyID or false
		$select = "SELECT u.email,
			l.firstname,
			l.lastname,
			l.countrycode,
			l.locationID,
			l.mob1,
			l.mob2
		FROM user u, landlord l
		WHERE u.userID = $this->userID
			AND l.landlordID = $this->userID";
		$this->set_qry_select($select);

		// Set the update query or false
		$update = "UPDATE landlord l
		INNER JOIN user u ON l.landlordid = u.userid
		SET u.email = :email,
			l.firstname = :firstname,
			l.lastname = :lastname,
			l.countrycode = :countrycode,
			l.locationID = :locationID,
			l.mob1 = :mob1,
			l.mob2 = :mob2
		WHERE u.userid = :userID";			

		$this->set_qry_update($update);
		// Set the insert query to false
		$this->set_qry_insert(0);
	}
} // End class Form
