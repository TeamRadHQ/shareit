<?php
/**
 * This file contains the FormProperty Form class.
 */
namespace ShareIt\Form;
// Helpers
use TeamRad\Helpers\Opt as Opt;
use TeamRad\Helpers\Cnd as Cnd;
// Form FieldSets
use ShareIt\Form\Fieldsets\PropertyDescription as Description;
use ShareIt\Form\Fieldsets\Address as Address;
use ShareIt\Form\Fieldsets\LocationPicker as Location;
use ShareIt\Form\Fieldsets\PropertyBedroom as Bedroom;
use ShareIt\Form\Fieldsets\PropertyFacilities as Facilities;

/**
 * FormProperty extends Form and defines a form which 
 * allows a landlord to add or update a property listing. 
 * It collects and validates data and then inserts or 
 * updates it in a database. 
 */
class FormProperty extends Form {
	/**
	 * Sets the propertyID and adds fieldsets. Calls the parent
	 * constructor and then sets field values from the database
	 * or POST data. If there is POST data, it is validated and 
	 * inserted or updated in the database. 
	 */
	public function __construct() {
		// Set the propertyID
		$this->set_propertyID();
		// Create and add Description, Location, Bedroom and Facilities fieldsets
		$this->set_fieldset(new Description());
		$this->set_fieldset(new Location());
		$this->set_fieldset(new Address());
		$this->set_fieldset(new Bedroom());
		$this->set_fieldset(new Facilities());
		// Call the parent constructor before setting values
		parent::__construct();
		// Get the property data from the database if there is no $_POST data		
		if (count($_POST) < 1) {
			$property = $this->get_data();
			if (is_array($property)) {
				$this->set_values($property);
				$this->landlordID = $property["landlordID"];
			} else {
				$this->landlordID = $this->set_userID(); 
			}// End if($property) 
			// $this->prep_db_values();
		} // end if(count)

		// Set the form action
		$this->set_action("edit.php");
		$this->set_POST_values();
		if (count($_POST) > 0 && $this->error_count < 1) {
			if ($this->propertyID) {
				$this->update_data();
			} else {
				$this->insert_data();
			}
		}
	}
	/**
	 * This method binds the field input values before
	 * calling the parent update_data() method. Upon success
	 * it redirects the user back to the edit page and
	 * displays a success mesage.
	 * @return redirect Redirects the user to a success message.
	 */
	protected function update_data() {
		$values = $this->prep_db_values();
		$values[":propertyID"] = $this->propertyID;
		// Update the record by running parent method.
		echo "<pre>", print_r($values), print_r($this->qry_update), "</pre>";
		parent::update_data($values);
		// redirect the user
		header("Location: " . $this->action . "&insert=1");
		die();
	}
	/**
	 * This method binds the field input values before
	 * calling the parent insert_data() method. Upon success
	 * it redirects the user back to the edit page and
	 * displays a success mesage.
	 * @return redirect Redirects the user to a success message.
	 */
	protected function insert_data() {
		$values = $this->prep_db_values();
		// Add the landlordID
		$values[":landlordID"] = $_SESSION["userID"];
		// Insert the record and get the id.
		$last_id = parent::insert_data($values);
		// Redirect the user
		header("Location: http://shareit.org/edit.php?insert=1?&propertyID=".$last_id);
		die();
	}
	/**
	 * This method collects and binds data required for
	 * database insert and update queries. 
	 * @return array An array of bound variables.
	 */
	protected function prep_db_values() {
		// Call parent method to get the values
		$values = parent::prep_db_values();
		// Remove countrycode as it's not used in the queries
		unset($values[":countrycode"]);
		// Set bedsingle and beDouble to 0 if default
		if (!$values[":bedsingle"]) 
			$values[":bedsingle"] = 0;
		if (!$values[":beddouble"]) 
			$values[":beddouble"] = 0; 
		return $values;
	}
	/**
	 * Sets the default queries for this form. 
	 * If a propertyID exists, it sets SELECT and UPDATE queries.
	 * If a propertyID does not exist, it sets an INSERT query. 
	 */
	protected function form_queries() {
		// Set select query if there is a propertyID or false
		$select = 0;
		if($this->propertyID) {
		$select = "SELECT p.propertyID, 
			p.landlordID,
			p.comments,
			p.costperday,
			p.street,
			p.suburb, 
			p.postcode,
			p.bedrooms,
			p.bedsingle,
			p.beddouble,
			p.kitchen,
			p.laundry,
			p.toilet,
			p.locationID,
			c.countrycode
		FROM property p, country c, location l
		WHERE propertyID = " . $this->propertyID . "
			AND p.locationID = l.locationId
			AND l.countryCode = c.countrycode";
	}
		$this->set_qry_select($select);
		// Set the update query or false
		$update = 0;
		// Set the update query if there is a propertyID
		if($this->propertyID) {
			$update = "UPDATE property 
			SET comments = :comments,  
			costperday = :costperday,  
			locationID = :locationID,  
			street = :street,  
			suburb = :suburb,  
			postcode = :postcode,  
			bedrooms = :bedrooms,  
			bedsingle = :bedsingle,  
			beddouble = :beddouble,  
			kitchen = :kitchen,  
			laundry = :laundry,  
			toilet = :toilet 
			WHERE propertyID = :propertyID";			
		}
		$this->set_qry_update($update);
		// Set the insert query or false
		$insert = 0;
		// Set the INSERT query if there isn't a propertyID
		if(!$this->propertyID) {
			$insert = "INSERT INTO property (landlordID, comments, costperday, street, suburb, postcode, bedrooms, bedsingle, beddouble, kitchen, laundry, toilet, locationID) 
									 VALUES(:landlordID, :comments, :costperday, :street, :suburb,:postcode,:bedrooms,:bedsingle,:beddouble,:kitchen,:laundry,:toilet,:locationID)";
		} 
		$this->set_qry_insert($insert);
	}
} // End class Form
?>