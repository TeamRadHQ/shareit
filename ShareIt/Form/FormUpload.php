<?php
/**
 * This file contains the FormUpload Form class.
 */
namespace ShareIt\Form;
/**
 * Namespace Aliases
 */
// Helpers
use \TeamRad\Helpers\Opt as Opt;
use \TeamRad\Helpers\Cnd as Cnd;
// Form FieldSets
use \ShareIt\Form\Fieldsets\UploadFile as UploadFile;
use \ShareIt\Form\Fieldsets\UploadProperty as Property;

/**
 * FormUpload extends the Form class.
 * It defines a form for uploading property image files to the 
 * shareIT database.   
 */
class FormUpload extends Form {
	/**
	 * The propertyID for the image being uploaded
	 * @var Integer
	 */
	protected $propertyID;
	/**
	 * The landlord's userID for property selection.
	 * @var Integer
	 */
	protected $landlordID;
	/**
	 * Set to the existing pictureID if an existing image is being edited.
	 * @var Integer
	 */
	protected $pictureID;
	/**
	 * A count of images for the current popertyID. This is set to ensure that the 
	 * form won't allow the user to insert additional images once the maximum is reached. 
	 * @var Integer
	 */
	protected $img_count;
	/**
	 * The name to set for the uploaded file. This is generated using the userID, propertyID, and image number.
	 * @var Integer
	 */
	protected $file_name=0;

	/**
	 * The constructor sets the propertyID and pictureID (if it exists) for the image being uploaded.
	 * It then gets an existing image count for the current property.
	 * It's then supposed to set the file name of the uploaded file. However, there's a propblem here. 
	 * It then adds fields for selecting a propety and uploading a file.
	 * If no data has been submitted and an existing file is being updated, it loads the picture data. 
	 * If form data is submitted, it runs validation and then inserts or updates a record.
	 */
	public function __construct() {
		$this->set_fieldset(new Property());
		$this->set_fieldset(new UploadFile());
		parent::__construct();
		$this->setter();
		$this->insert_data();
		$this->update_data();
}
	/**
	 * Runs the set methods for this Form. 
	 */
	public function setter() {
		// Get the propertyID
		$this->set_propertyID();
		// Get the pictureID
		$this->set_pictureID();
		// set the form action
		$this->set_action('upload.php');
		$this->set_file_name();
		$this->form_queries();
		$image = $this->get_data();
		if (is_array($image)) {
			$this->set_values($image);
		}
	}
	/**
	 * Sets the file name for the file being uploaded by this form.
	 */
	public function set_file_name() {
		$file = $this->fieldsets['upload-an-image']->get_values();
		$this->file_name = $file["file"];
	}
	/**
	 * Runs the update query for an existing image.
	 * @return Redirect Redirects the user to a success message.
	 */	
	protected function update_data() {
		// Only start UPDATE if there is POST data and a pictureID
		if ( $this->pictureID && count($_POST) ) {
			// Prepare to INSERT data.
			$values = $this->prep_db_values();
			// Remove the propertyID (not in query)
			unset($values[":propertyID"]);
			// Set pictureID for update
			$values[":pictureID"] = $this->pictureID;			
			// Run the UPDATE query 
			parent::update_data($values);
			// Redirect the user
			header("Location: upload.php?propertyID=".$this->propertyID."&update=1");
			die();
		}
	}
	/**
	 * Runs the insert query for a new image upload. It also removes the bound pictureID, 
	 * as this is not used in the INSERT query. 
	 * @return Redirect Redirects the user to a success message on insert.
	 */
	protected function insert_data() {
		// Only start INSERT if there is POST data AND no pictureID 
		if ( !$this->pictureID && count($_POST) ) {
			// Prepare to INSERT data.
			$values = $this->prep_db_values();
			parent::insert_data($values);
			// Redirect the user
			header("Location: upload.php?insert=1&propertyID=" . $this->propertyID);
			die();
		}
	}
	/**
	 * Sets the default queries for this form. 
	 * If a picturedID exists, it sets SELECT and UPDATE queries.
	 * If a picturedID does not exist, it sets an INSERT query. 
	 */
	protected function form_queries() {
		// Set select query if there is a propertyID or false
		$select = 0;
		if($this->pictureID) {
		$select = 	"SELECT propertyID, photoname as file, comments, featuredImage FROM picture WHERE pictureID = $this->pictureID";
	}
		$this->set_qry_select($select);
		// Set the update query or false
		$update = 0;
		// Set the update query if there is a propertyID
		if($this->pictureID) {
			$update = "UPDATE picture SET photoname = :file, comments = :comments, featuredimage = :featuredImage WHERE pictureID = :pictureID";			
		}
		$this->set_qry_update($update);
		// Set the insert query or false
		// Set the INSERT query if there isn't a propertyID
		$insert = "INSERT INTO picture (propertyID, photoname, comments, featuredimage) VALUES(:propertyID, :file, :comments, :featuredImage)";
		$this->set_qry_insert($insert);
	}
	/**
	 * This method is supposed to correctly set the upload name. However, it's 
	 * defaulting to NULL on a new image upload. 
	 * @todo Clean this mess up!!!
	 */
	protected function set_upload_name() {}
	/**
	 * Binds form data for database queries.
	 * @todo Continue Debugging the upload name issue.
	 * @return Array An array of bound data fields.
	 */
	// protected function prep_db_values() {}
} // End class Form
