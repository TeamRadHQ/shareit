<?php
/**
 * This file adds functionality to the Fieldset 
 * class for use on the ShareIt website. 
 */
namespace ShareIt\Form;
use \TeamRad\Helpers as Helpers;/**
 * The FieldSet class can be used to collect a set of fields
 * for use in other applications. 
 * @uses \TeamRad::Form::Form
 * @uses \TeamRad\Helpers
 */
class FieldSet extends \TeamRad\Form\FieldSet {
	
	/**
	 * Calls the parent class constructor.
	 * @param boolean $required True if all fields are required.
	 */
	public function __construct($required=false) {
		parent::__construct($required);
	}
	/**
	 * The propertyID for the image being uploaded
	 * @var Integer
	 */
	protected $propertyID;
	/**
	 * The landlord's userID for property selection.
	 * @var Integer
	 */
	protected $userID;
	/**
	 * Set to the existing pictureID if an existing image is being edited.
	 * @var Integer
	 */
	protected $pictureID;
	/**
	 * This method sets the form's userID from the $_SESSION data. 
	 */
	public function set_userID() {
		$this->userID = $_SESSION["userID"];
	}
	/**
	 * Returns the pictureID if its set
	 */
	protected function set_pictureID() {
		if (isset($_GET["pictureID"])) 
			$this->pictureID = $_GET["pictureID"];
	}
	/**
	 * Returns the propertyID if its set
	 */
	protected function set_propertyID() {
		if (isset($_GET["propertyID"])) 
			$this->propertyID = $_GET["propertyID"];
	}

} // end class
?>
