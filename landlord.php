<?php
/**
 * The admin page allows registered uses to view their user profile and 
 * any properties which they have added. If a user is not logged in and 
 * they try to access this page, they will be redirect to the index page.
 */
namespace ShareIt;
require_once('autoload.php');
use TeamRad\Helpers as Helpers;
// Get a database connection
$db = new DB;
$landlordID = set_landlordID();
$properties = landlord_properties($landlordID);
// Set the page title
$page_title = "Properties for this landlord";
// Add the page header
add_head("$page_title");
?>
<section class="col-md-9">
<?php
property_table($properties);
?>
</section>
<?php add_JS();?>
</body>
</html>