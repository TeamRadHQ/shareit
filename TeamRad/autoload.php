<?php
/**
 * This is the autoload script. It loads all of the required assets for 
 * the ShareIT website. It starts by requiring vendor/autoload which is a
 * PHP Composer autoloader for including third party modules. It then registers
 * a class autoloader which includes all namespaced classes in this project. 
 * Finally, helper classes for TeamRad and ShareIT assets are required.
 */

/**
 * This is the class autoloader. It looks in each subfolder and 
 * automatically loads any class file which is properly namespaced. 
 * This is achieved by storing Classes in subfolders with corresponding 
 * namespaces. For example, a class in the namespace "Vendor\PackageName"
 * should be located in "/Vendor/PackageName". 
 * @param  string   $className  The name of the class being loaded.
 * @return require              Requires the class files.
 */
function autoload_teamrad($className)
{
    $className = ltrim($className, '\\');
    $fileName  = '';
    $namespace = '';
    if ($lastNsPos = strrpos($className, '\\')) {
        $namespace = substr($className, 0, $lastNsPos);
        $className = substr($className, $lastNsPos + 1);
        $fileName  = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
    }
    $fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';
    if ($namespace != 'Shareit') return;
        require $fileName;
}
spl_autoload_register('autoload_teamrad');
// Required Functions
require_once('Helpers/functions.mustache.php');
require_once('functions.php');
require_once('Form/Form.php');
?>
