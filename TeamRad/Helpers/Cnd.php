<?php 
/**
 * This file contains the Cnd Helper class.
 */
namespace TeamRad\Helpers;

/**
 * This is a helper class for defining validation conditions. 
 * It is used by the Field Class and its children. 
 */
class Cnd {
	/**
	 * Use to define a minimum value for a field
	 * @param  float $val  	A numeric value to set as the minimum.
	 * @return array      	A key value array.
	 */	
	public static function min($val) {
		return array("min_value" => $val);
	}
	/**
	 * Use to define a maximum value for a field
	 * @param  float $val  	A numeric value to set as the maximum.
	 * @return array      	A key value array.
	 */	
	public static function max($val) {
		return array("max_value" => $val);
	}
	/**
	 * Use to define a minimum value and maximum for a field.
	 * These values can be passed in any order. The method will
	 * determine their order.
	 * @param  float  $val1 The minimum or maximum value.
	 * @param  float  $val2 The minimum or maximum value.
	 * @return array        An array of keys and values.
	 */
	public static function btwval($val1, $val2) {
		$vals = self::minmax($val1, $val2);	
		return array_merge(
			self::min($vals["min"]),
			self::max($vals["max"]));
	}
	/**
	 * Sets a positive value condition for a field
	 * @return Array A positive value condition.
	 */	
	public static function pos() {
		return array("is_postive" => true);
	}
	/**
	 * Sets a negative value condition for a field
	 * @return Array A negative value condition.
	 */
	public static function neg() {
		return array("is_negative" => true);
	}
	/**
	 * Set an exact length for this field.
	 * @param  Integer $val The exact length for a field.
	 * @return Array      An exact length condition.
	 */
	public static function len($val) {
		return array("exact_length" => $val);
	}
	/**
	 * Sets a minimum length for a field.
	 * @param  Integer $val The minimum length for a field.
	 * @return Array        A minimum length condition.
	 */
	public static function minlen($val) {
		return array("min_length" => $val);
	}
	/**
	 * Sets a maximum length for a field.
	 * @param  Integer $val The maximum length allowed.
	 * @return Array        A maximum length condition.
	 */
	public static function maxlen($val) {
		return array("max_length" => $val);
	}
	/**
	 * Use to define a minimum and maximum length for a field.
	 * These values can be passed in any order. The method will
	 * determine their correct order.
	 * @param  float  $val1 The minimum or maximum length.
	 * @param  float  $val2 The minimum or maximum length.
	 * @return array        An array of keys and values.
	 */
	public static function btwlen($val1, $val2) {
		$vals = self::minmax($val1, $val2);	
		return array_merge(
			minlen($vals["min"]),
			maxlen($vals["max"]));
	}
	/**
	 * Specifies a string which must be contained in a 
	 * field's input.
	 * @param  String $val 	A string which must be contained in a field.
	 * @return Array      	A contains string condition.
	 */
	public static function contains($val) {
		return array("str_contains" => $val);
	}
	/**
	 * Specifies a string which must NOT be contained in a 
	 * field's input.
	 * @param  String $val 	A string which must NOT be contained in a field.
	 * @return Array      	A NOT contains string condition.
	 */
	public static function not($val) {
		return array("str_not_contains" => $val);
	}
	/**
	 * This method can be used to build multiple condition
	 * Arrays into a single array which can be passed at 
	 * construction. Just dump each array in an array, or call
	 * each method in an array and this method will merge them
	 * and pass them back.
	 * @param  Array $array  An array of single conditions.
	 * @return Array         A merged array of conditions.
	 */
	public static function build($array) {
		$conditions=array();
		foreach(array_keys($array) as $key) {
			$conditions = array_merge($conditions, $array[$key]);
		}
		return $conditions;
	}
	/**
	 * Sorts two values.
	 * @param  Float $val1  The first value.
	 * @param  Float $val2  The second value.
	 * @return Array        An array containing "min" and "max".
	 */
	private static function minmax($val1, $val2) {
		if ($val1 < $val2 || $val1 == $val2) { //Val1 is min
			return array("min" => $val1, "max" => $val2);
		} else { // Val2 is min
			return array("min" => $val2, "max" => $val1);
		}
	}// end minmax()
} // End class
?>