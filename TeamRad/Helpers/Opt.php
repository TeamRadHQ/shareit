<?php 
/**
 * Contains a set of methods for generating various option/value
 * pairs for use in the Field class and its children.
 */
namespace TeamRad\Helpers;

use ShareIt\DB as DB;

/**
 * Returns an option, value array.
 * This can be used to add options for select fields, radio
 * buttons and checklists.
 */
class Opt {
	/**
	 * Returns an array of numbers
	 * @param  Integer $start The start time (24H)
	 * @param  Integer $end   The end time (24H)
	 * @return Array          Block of hours
	 */
	public static function numbers($start, $end) {
		$rng = self::minmax($start, $end);
		for ($i=$rng["min"]; $i<=$rng["max"];$i++) {
			$numbers[] = self::values($i, $i);
		}
		return $numbers;
	}
	/**
	 * Returns an array of hour blocks
	 * @param  Integer $start The start time (24H)
	 * @param  Integer $end   The end time (24H)
	 * @return Array          Block of hours
	 */
	public static function hr_blocks($start, $end) {
		$rng = self::minmax($start, $end);
		for ($i=$rng["min"]; $i<=$rng["max"];$i++) {
			$j = $i;
			if ($j > 12)	
				$j += -12;
			$value = $j . ":00 - " . ($j+1) . ":00";
			$hours[] = values($i, $value);
		}
		return $hours;
	}
	/**
	 * Outputs an array of option values for days of the week
	 * @param  integer $start The start day integer value.
	 * @param  integer $end   The end day integer value.
	 * @return Array An array of option value pairs.
	 */
	public static function days($start, $end) {
		$rng = self::minmax($start, $end);
		for ($i=$rng["min"]; $i<=$rng["max"];$i++) {
				$day = jddayofweek($i-1,1);
				$days[] = self::values($i, $day);
			}
		return $days;
	}
	/**
	 * Returns a week's worth of option/values
	 * starting on a Monday and ending on a Sunday.
	 * @return Array An array of days of the week.
	 */
	public static function week() { 
		return self::days(1,7); 
	}
	/**
	 * Returns a work week's worth of option/values
	 * starting on a Monday and ending on a Friday.
	 * @return Array An array of days of the week.
	 */
	public static function busweek() { 
		return self::days(1,5); 
	}
	/**
	 * Returns a weekend's worth of option/values
	 * starting on a Saturday and ending on a Sunday.
	 * @return Array An array of days of the week.
	 */
	public static function weekend() { 
		return self::days(6,7); 
	}
	/**
	 * Outputs an array of month integers and names.
	 * @param  Integer $start The start month
	 * @param  Integer $end   The end month
	 * @return Array          The months
	 */
	public static function month($start, $end) {
		$rng = self::minmax($start, $end);
		for ($i=$rng["min"]; $i<=$rng["max"];$i++) {
				$month = date("F", mktime(0, 0, 0, $i, 10));
				$months[] = values($i, $month);
			}
		return $months;
	}
	/**
	 * Generates a year's worth of option values.
	 * @return Array An array of months of the year.
	 */
	public static function year() { 
		return self::month(1, 12); 
	}
	/**
	 * Generates month option values for the first quarter.
	 * @return Array An array of months of the year.
	 */
	public static function year_q1() { 
		return self::month(1, 3); 
	}
	/**
	 * Generates month option values for the second quarter.
	 * @return Array An array of months of the year.
	 */
	public static function year_q2() { 
		return self::month(4, 6); 
	}
	/**
	 * Generates month option values for the third quarter.
	 * @return Array An array of months of the year.
	 */
	public static function year_q3() { 
		return self::month(7, 9); 
	}
	/**
	 * Generates month option values for the fourth quarter.
	 * @return Array An array of months of the year.
	 */
	public static function year_q4() { 
		return self::month(10, 12); 
	}
	/**
	 * Outputs an array of opt values for Australian states
	 * @return Array An array of Australian states.
	 */
	public static function states() {
		$states[] = self::values('ACT', 'Australian Capital Territory');
		$states[] = self::values("NSW", "New South Wales");
		$states[] = self::values("NT", "Northern Territory");
		$states[] = self::values("QLD", "Queensland");
		$states[] = self::values("SA", "South Australia");
		$states[] = self::values("TAS","Tasmania");
		$states[] = self::values("VIC", "Victoria");
		$states[] = self::values("WA", "Western Australia");
		return $states;
	}
	/**
	 * Generates a set of common honorifics.
	 * @return Array An array of honorifics.
	 */
	public static function titles() {
		$titles=[];
		$titles[] = self::values('Mr','Mr');
		$titles[] = self::values('Mrs','Mrs');
		$titles[] = self::values('Miss','Miss');
		$titles[] = self::values('Ms','Ms');
		$titles[] = self::values('Dr','Dr');
		$titles[] = self::values('Prof','Prof');
		$titles[] = self::values('Hon','Hon');
		return $titles;
	}

	/**
	 * Generates a list of skill levels.
	 * @return Array An array of skill levels.
	 */
	public static function skill_lvl() {
		$skill[] = self::values("BEG", "Beginner");
		$skill[] = self::values("NOV", "Novice");
		$skill[] = self::values("INT", "Intermediate");
		$skill[] = self::values("EXP", "Expert");
		$skill[] = self::values("MST", "Master");
		return $skill;
	}
	/**
	 * Returns an option value array
	 * @param  String $value  The value for the option
	 * @param  String $option The text for the option
	 * @return Array          Array("Option", "Value")
	 */
	public static function values($value, $option) {
		return array(
				"value"		=> $value, 
				"option"	=> $option);
	} // end values()
	/**
	 * Runs a SELECT query on the database and returns the first 
	 * two columns as value [1] and option [2]. 
	 * @param  String $sql A SELECT statement where the first column is 
	 *                     the value to set and the second column is the 
	 *                     option to display.
	 * @return Array       An array of options and values from a database.
	 */
	public static function db_values($sql) {
		// Get the global database connection
		$db = new DB;
		$options = array();
		// Get a list of locations 
		$results = $db->query($sql);
		while($row = $results->fetch(DB::FETCH_NUM)) {
			$options[]= self::values($row[0],$row[1]);
		}// end while($results)
		return $options;
	}// end db_values()

	/**
	 * Returns an array with min and max keys
	 * @param  Float  $val1 The first value
	 * @param  Float  $val2 The second value
	 * @return Array       	Sorts values into "min" and "max"
	 */
	private static function minmax($val1, $val2) {
		if ($val1 < $val2 || $val1 == $val2) { //Val1 is min
			return array("min" => $val1, "max" => $val2);
		} else { // Val2 is min
			return array("min" => $val2, "max" => $val1);
		}
	}// end minmax()

} // End class
?>