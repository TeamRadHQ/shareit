<?php
/**
 * This file contains the Vld Helper class.
 */
namespace TeamRad\Helpers;
/**
 * Vld is a static Helper class which contains method for validating 
 * input data. 
 */
class Vld {
/**
 *  required() validation
 *  The following functions are use to validate whether a required
 *  field is populated or not.
 */
	/**
	 * Checks a field to see if it's required and not empty. If a field is 
	 * empty and required, an error will be added to the $errors array. 
	 * The function will always return false on an empty field whether it's 
	 * required or not. This is to signal to the validator to stop any 
	 * further validation on this field. 
	 * @param  Array 	$field 	The field to be tested
	 * @return Boolean        	Returns true if the field is not empty.
	 */
	public static function required($field) {
		switch($field->type()) {
			case "select":
			case "radio":
				return self::req_sel($field); break;
			case "checkbox":
				return self::req_chk($field); break;
			default:
				return self::req_def($field); break;
		}
	}
	/**
	 * This is the default check for a required field. If a field is empty
	 * and required, an error will be added to the $errors array. 
	 * @param  Array 	$field 	The field being tested.
	 * @return Boolean        	Returns true if the field is not empty.
	 */
	public static function req_def($field) {
		$pass = false;
		$id = $field->id();
		if (!$field->required()) { // If it's not required
			$pass = true; // The field passes
		} else { // Otherwise it's required
			if (!empty( $field->value() )) // So if it's not empty
				 $pass = true; // The field passes
		} // End if else $field["required"]
		return $pass;
	} // end req_def()
	/**
	 * This is the check for a required field which has a "checkbox" type. 
	 * It goes through each option and if none are checked it returns false.
	 * If no boxes are checked and the field is required, it also adds an 
	 * error.
	 * @param  Array 	$field 	The field being tested.
	 * @return Boolean        	True if the field is not empty.
	 */
	public static function req_chk($field) {
		$pass = false;
		if($field->values()) { // Check the values against options
			foreach($field->get_options() as $option) {
				if (in_array($option["value"] , $field->values())) {
					$pass = true;
				}
			}
		} // End if (values)
		return $pass;
	} // end req_chk()	
	
	/**
	 * This is the check for a required field which has a "select" or "radio" type. 
	 * It checks each option and returns true if one is selected. If no option
	 * is selected and the field is required, an error is added to the $errors
	 * array. 
	 * @param  Array 	$field 	The field to be tested.
	 * @return Boolean        	Returns true if the field is not empty.
	 */
	public static function req_sel($field) {
		if (!$field->required())
			return true;
		$pass = false;
		foreach($field->get_options() as $option) {
			if ($option["value"] == $field->value())  { 
				$pass = true;
				break;
			}
		} // end foreach($option)
		return $pass;
	} // end req_sel()
////////END IS REQUIRED FUNCTIONS////////////////

/**
 *  Type validation
 *  These functions will check a field against its type
 */
	/**
	 * Returns true if the field is a valid integer.
	 * @param  Array  	$field 		The field being tested.
	 */
	public static function int($field) {
		if (filter_var($field->value(), FILTER_VALIDATE_INT)) 
			return true;
	} // end vld_int()
	/**
	 * Returns true if the field is a valid float.
	 * @param  Array 	$field 	The field being tested.
	 */
	public static function float($field) {
		if (filter_var($field->value(), FILTER_VALIDATE_FLOAT)) 
			return true;
	} // end vld_flt()
	/**
	 * Returns true if the field is a valid email address.
	 * @param  Array 	$field 	The field being tested.
	 */
	public static function email($field) {
		if (filter_var($field->value(), FILTER_VALIDATE_EMAIL)) 
			return true;
	}
	/**
	 * Returns true if a field is a valid url.
	 * @param  Array 	$field 	The field being tested.
	 */
	public static function url($field) {
		if (filter_var($field->value(), FILTER_VALIDATE_URL)) 
			return true;
	}
	/**
	 * Returns true if the field is a valid date in DD/MM/YYYY format.
	 * @param  Array 	$field 	The field being tested.
	 */
	public static function date($field) {
	$date = $field->value(); $is_date = false;
	if (strlen($date) === 10 && substr_count($date, '/', 2, 4) === 2) { 
		$date  = explode('/', $date);
		if (checkdate($date[1], $date[0], $date[2]))
       		$is_date = true;
		} // end if $date	
		return $is_date;
	} // End is_date()
	/**
	 * Returns true if a field is numeric.
	 * @param  Array 	$field 	The field being tested.
	 */
	public static function numeric($field) {
		if (is_numeric($field->value())) 
			return true;
	}
	/**
	 * Returns true if a field only contains non-spaced alphabetic characters.
	 * @param  Array 	$field 	The field being tested.
	 */
	public static function txt_only($field) {
		if (self::tst_txt($field->value())) 
			return true;
	}
	/**
	 * Returns true if a field only contains alphabetic characters and spaces.
	 * @param  Array 	$field 	The field being tested.
	 */
	public static function txt_spc($field) {
		// Strip spaces from the string and run $this->txt_only()
		if (self::tst_txt(str_replace(' ', '', $field->value()))) 
			return true;
	}
	/**
	 * Tests a string and if it only contains alphabetic characters, 
	 * it returns true.
	 * @param  String 	$string 	The string to be tested
	 * @return Boolean        		True if there's only alpha characters
	 */
	public static function tst_txt($value) {
		if (ctype_alpha($value)) 
			return true;
	}
	/**
	 * Tests a string and if it only contains alphanumeric characters, 
	 * it returns true.
	 * @param  String $string The string to be tested
	 * @return Boolean        True if there's only alphanumeric characters
	 */
	public static function tst_alnum($value) {
		if (ctype_alnum($value)) 
			return true;
	}
	/**
	 * Returns true if field only contains alphanumeric characters.
	 * @param  Array 	$field 	The field being tested.
	 */
	public static function alnum($field) {
		if (self::tst_alnum(($field->value()))) 
			return true;
	}
	/**
	 * Returns true if field only contains alphanumeric characters and spaces.
	 * @param  Array 	$field 	The field being tested.
	 */
	public static function alnum_spc($field) {
		$value = $field->value();
		if (self::tst_alnum(strip_spaces($value))) 
			return true;
	}// End alphanum_spaces()
////////END TYPE VALIDATION////////////////

/**
 *  Conditional validation
 *  The functions that follow are for validating fields based on 
 *  their conditions (eg value/length constraints)
 */
	/**
	 * Gets the value for the field condition you want to test.
	 * @param  Array 	$field      The field you're testing
	 * @param  String 	$condition 	The condition whose value you want to get
	 * @return String   	        The test value for the $condtion
	 */
	public static function get_condition($field, $condition) {
		$conditions = $field->get_conditions();
		return $conditions[$condition];
	}
	/**
	 * Returns true if field is greater than or equal to condition.
	 * @param  Array 	$field      The field you're testing
	 */
	public static function minval($field) {
		$condition = self::get_condition($field, "min_value");
		$value = $field->value();
		if ($value >= $condition) 
			return true;
	} // end min_value()
	/**
	 * Returns true if field is less than or equal to condition.
	 * @param  Array 	$field      The field you're testing
	 */
	public static function maxval($field) {
		$condition = self::get_condition($field, "max_value");
		$value = $field->value();
		if ($value <= $condition) 
			return true;
	}
	/**
	 * Returns true if the field is positive
	 * @param  Array 	$field      The field you're testing
	 */
	public static function ispos($field) {
		if ($field->value() > 0) 
			return true;
	}
	/**
	 * Returns true if a value is negative.
	 * @param  Array 	$field      The field you're testing
	 */
	public static function isneg($field) {
		if ($field->value() < 0) 
			return true;
	}
	/**
	 * Returns true if a string is at least as long as condition.
	 * @param  Array 	$field      The field you're testing
	 */
	public static function minlen($field) {
		$condition = self::get_condition($field, "min_length");
		if(in_array($field->type(), ['select','radio','checkbox']))
			// Add handling for these types later.
			return;
		if (strlen($field->value()) >= $condition) 
			return true;
	}
	/**
	 * Returns true if a string is no longer than condition.
	 * @param  Array 	$field      The field you're testing
	 */
	public static function maxlen($field) {
		$condition = self::get_condition($field, "max_length");
		if(in_array($field->type(), ['select','radio','checkbox']))
			// Add handling for these types later.
			return;
		if (strlen($field->value()) <= $condition) 
			return true;
	}
	/**
	 * Returns true if a string is exactly as long as condition.
	 * @param  Array 	$field      The field you're testing
	 */
	public static function exlen($field) {
		$condition = self::get_condition($field, "exact_length");
		if(in_array($field->type(), ['select','radio','checkbox']))
			// Add handling for these types later.
			return;
		if (strlen($field->value()) == $condition) 
			return true;
	}
	/**
	 * Returns true if a string contains condition.
	 * @param  Array 	$field      The field you're testing
	 */
	public static function contains($field) {
		$string = '_'.$field->value(); // Add underscore at - index
		$sub_string = self::get_condition($field, "str_contains");
		if (strpos($string, $sub_string) >= 1 ) 
			return true;
	}
	/**
	 * Returns true if a string does not contain a substring.
	 * @param  Array 	$field      The field you're testing
	 */
	public static function not_contains($field) {
		$string = '_'.$field->value(); // Add underscore at - index
		$sub_string = self::get_condition($field, "str_not_contains");
		if (strpos($string, $sub_string) < 1 ) 
			return true;
	}	
////////END CONDITIONAL VALIDATION////////////////
}// Class End
/**
 * A helper function for stripping spaces from strings.
 * @param  String $string The string to be stripped.
 * @return String         $string without the spaces.
 */
function strip_spaces($string) {
	return str_replace(' ', '', $string);
}
?>