<?php
/**
 * This file contains the TeamRad Form class.
 */
namespace TeamRad\Form;
use \TeamRad\Helpers as Helpers;

use \ShareIt\DB as DB;


/**
 * TeamRad/Form is a class which can be used to create a 
 * fieldset, render a form, collect user input, validate 
 * its data and provide information back to the user. 
 * The form is generated using the Mustache template 
 * engine. TeamRad\Form uses the Fieldset class for storing
 * field data.
 */
class Form {
	/**
	 * Stores the form method being used to submit this form's data.
	 * @var string
	 */
	protected $method;
	/**
	 * Stores the form action url
	 * @var string
	 */
	protected $action;
	/**
	 * The SELECT query for getting this form's data.
	 * @var string
	 */
	protected $qry_select;
	/**
	 * The UPDATE SQL query for this form's data.
	 * @var string
	 */
	protected $qry_update;
	/**
	 * The INSERT SQL query for this form's data.
	 * @var string
	 */
	protected $qry_insert;
	/**
	 * An array which contains the entire form contents. This 
	 * array is sent to Mustache, which renders the form to the 
	 * browser.
	 * @var array
	 */
	protected $form = array();
	/**
	 * An array of fieldset objects. Each of these fieldsets is
	 * rendered to the browser in the order they are added to 
	 * this array.
	 * @var array
	 */
	public $fieldsets = array();
	/**
	 * Contains the Mustache Template object. Use this object to 
	 * pass arrays to Mustache that are rendered to the browser.
	 * @var \Mustache\Mustache_Engine
	 */
	protected $templates;
	/**
	 * A count of total validation errors 
	 * @var int
	 */
	protected $error_count;
	/**
	 * The constructor instantiates a new Mustache template object.
	 * @todo Add ability to pass options to constructor.
	 */
	public function __construct() {
		// Add Mustache templates
		$this->templates =  new \Mustache_Engine(array(
			'loader' => new \Mustache_Loader_FilesystemLoader(__DIR__.'/templates/'),
		));   
		// Set the form queries
		$this->form_queries();
		// Set form defaults
		$this->set_method();
		$this->set_action(); 
	} // end __construct()

	/**
	 * Sets the default queries for this form
	 */
	protected function form_queries() {
		if(!$this->qry_select && !$this->qry_update && !$this->qry_insert) {
			// Set default queries
			$this->set_qry_select();
			$this->set_qry_update();
			$this->set_qry_insert();
		} // End if
	} // End form queries

	/**
	 * Sets the SELECT query for this form or null if it doesn't have one.
	 * @param String $sql An SQL SELECT statement for populating data. This must return named fields matching the form field ids.
	 */
	protected function set_qry_select($sql=null) {
		// If sql is null and qry_select is empty, or sql is not empty
		if( (!$sql && !$this->qry_select) || $sql )
			$this->qry_select = $sql;
	}
	/**
	 * Sets the UPDATE query for this form or null if it doesn't have one.
	 * @param String $sql An SQL UPDATE statement for updating the database with form data. 
	 */	
	protected function set_qry_update($sql=null) {
		// If sql is null and qry_update is empty, or sql is not empty
		if( (!$sql && !$this->qry_update) || $sql )
			$this->qry_update = $sql;
	}
	/**
	 * Sets the INSERT query for this form or null if it doesn't have one.
	 * @param String $sql An SQL INSERT statement for updating the database with form data. 
	 */
	protected function set_qry_insert($sql=null) {
		// If sql is null and qry_insert is empty, or sql is not empty
		if( (!$sql && !$this->qry_insert) || $sql )
			$this->qry_insert = $sql;
	}
	/**
	 * Sets the method for this form.
	 * @param string $method The method of the form. Can be "post" or "get".
	 */
	public function set_method($method="post") {
		if (!in_array(strtolower($method), ["post","get"] )) 
			$method = "post";
		$this->method = $method;
	}// End set method	
	/**
	 * Sets the action for this form.
	 * @param string $action The action of the form. Can be "post" or "get".
	 */
	public function set_action($action="") {
		$this->action = $action;
	}// End set action
	/**
	 * Takes a fieldset object and adds or updates it.
	 * @param FieldSet $fieldset An instantiated FieldSet object.
	 */
	public function set_fieldset($fieldset) {
		$this->fieldsets[$fieldset->id()] = $fieldset;
  	} // end set_fieldset;
  	/**
  	 * Loops through each fieldset and calls its set_POST_values()
  	 * method. 
  	 */
  	public function set_POST_values() {
  		$this->set_values($_POST);
  	}// end set_POST_values()
  	
  	/**
  	 * Runs set_values() on each fieldset.
  	 * @param array $values An array of values to set.
  	 */
  	public function set_values($values = []) {
  		foreach(array_keys($this->fieldsets) as $set) {
  			$this->fieldsets[$set]->set_values($values);
  			$this->error_count += $this->fieldsets[$set]->error_count();
  		}// end foreach($set)
  	}// end set_POST_values()
  	/**
  	 * Loops through each fieldset and gets its value and adds it to an array
  	 * @return array An array of values.
  	 */
  	public function get_values() {
  		$values =[];
  		foreach(array_keys($this->fieldsets) as $set) {
  			$value = $this->fieldsets[$set]->get_values();
  			$values = array_merge($values, $value);
  		}// end foreach($set)
  		return $values;
  	}// end set_POST_values()

  	/**
  	 * Sets the form's method and action. Loads the form's fieldset. 
  	 * Adds a "success" flag, if there is form input and no validation 
  	 * errors.
  	 */
	public function set_form() {
		$this->form["action"] = $this->action;
		$this->form["method"] = $this->method;
		foreach($this->fieldsets as $set => $object) {
			$this->form["groups"][] = $this->fieldsets[$set]->get();
		}
		if(count($_POST) >= 1 && !$this->error_count) {
			$this->form["success"] = true;
		} 
		return $this->form;
	} // end set_form()
	/**
	 * Returns the number of validation errors found.
	 * @return integer The number of errors.
	 */
	public function errors() {
		return $this->error_count;
	}
	/**
	 * This form compiles the form data and renders it to the browser 
	 * using the Mustache template named /templates/form.mustache
	 * @return string Echoes html to browser.
	 */
  	public function render() {
  		echo $this->get();
  	}
  	/**
  	 * Prepares the form fields and data applies the Mustache
  	 * form template and then returns the output html.
  	 * @return html The form's html.
  	 */
  	public function get() {
  		$this->set_form();
  		return $this->templates->render('form', array("form" => $this->form));	
  	}
  	/**
  	 * Runs the SELECT query for this form to get existing data. Firstly, it checks
  	 * to see if there is a query set (meaning that there is existing data). If there 
  	 * is POST data then the method will stop. Otherwise, it runs the SELECT query to 
  	 * populate the data. 
  	 * @return Array An associative array of fields
  	 */
  	public function get_data() {
  		// Stop if there's no existing record.
		if(!$this->qry_select)
			return 0;
		// Stop if there's POST data.
		if(count($_POST))
			return;
		// Get the database.
		$db = new DB;
		// Select query for getting property.
		$result = $db->query($this->qry_select);
		// Set the result to array.
		$data = $result->fetch(DB::FETCH_ASSOC);
		return $data;
	}
 	/**
 	 * Runs the INSERT query for this form to create a new database record. 
 	 * @param  Array $values   An array of bound values for insertion.
 	 * @return integer         The ID of the last inserted record.
 	 */
	protected function insert_data($values) {
		$db = new DB;
		// Prepare the query
		$query = $db->prepare($this->qry_insert);
		// Execute
		$query->execute($values);
		// Get the last INSERT id and return it to the user.
		return $db->lastInsertId();
	}
	/**
	 * Runs the UPDATE query for this form to update an existing database
	 * record.
	 * @param  Array $values  An array of bound values for update.
	 */
	protected function update_data($values) {
		$db = new DB;
		// Prepare the query
		$query = $db->prepare($this->qry_update);
		// Execute
		$query->execute($values);
	}
	/**
	 * Returns a set of bound values for the form's field input.
	 * @return Array An array of bound values.
	 */
	protected function prep_db_values() {
		$values = $this->get_values();
		// Go through and bind the values to a new array
		foreach (array_keys($values) as $key) {
			$bound[":".$key] = $values[$key];
		}
		return $bound;
	}
 } // End class Form
?>