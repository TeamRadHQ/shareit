<?php
/**
 * This file defines the parent fieldset class. 
 */
namespace TeamRad\Form;
use \TeamRad\Helpers as Helpers;
require_once($_SERVER["DOCUMENT_ROOT"]."/TeamRad/Form/Validator.php");
/**
 * The FieldSet class can be used to collect a set of fields
 * for use in other applications. 
 * @todo Considering the inclusion of $key and $id in some methods, 
 * this class needs some review and restructure. There's no clear 
 * distinction between these two variables just from looking at the 
 * code. 
 */
class FieldSet {
	/**
	 * This array contains a collection of Field objects. 
	 * @uses \TeamRad\Form\Field
	 * @var array $fieldset
	 */
	public $fieldset = array();
	/**
	 * An ID for this fielset
	 * @var String
	 */
	protected $id;
	/**
	 * A display label for this fieldset. 
	 * @var string
	 */
	protected $label;
	/**
	 * A display message for this fieldset. 
	 * @var string
	 */
	protected $message;
	/**
	 * This property stores a validator object which is 
	 * used for field input validation. 
	 * @var TeamRad\Form\Validator 	A Validator object
	 */
	protected $validator;
	/**
	 * An array of validation errors received for this fieldset.
	 * @uses TeamRad\Form\Validator
	 * @var array
	 */
	public $error_count;
	/**
	 * True if all fields are required.
	 * False if all fields are not required.
	 * Note: Setting to false does not override field requirement 
	 * settings. So if a field is required, it will still be required
	 * if the fieldset's required flag is set to false.
	 * @var boolean 
	 */
	protected $required;
	/**
	 * Constructor sets required true or false.
	 * @param boolean $required If true, all fields are required. <br>
	 *                          If false, no fields are required.
	 */
	public function __construct($required=false) {
		$this->required = $required;
	}
	/**
	 * Returns the fieldset and its fields' properties in an html table.
	 * @return string An html table listing this object's properties. 
	 */
	public function __toString() {
		$string = "<table border=1><tr><td colspan=2><h1>Fieldset::".$this->label."</h1></tr></td>";
		$string.= "<tr><td><b>Message:</b></td><td>".$this->message."</td></tr>";
		$string.= "<tr><td><b>Fields:</b></td><td>".count($this->fieldset)."</td></tr>";
		foreach($this->fieldset as $field => $value) {
			$str_field = $this->fieldset[$field]->__toString();
			$str_field = str_replace(array("<table border=1>", "</table>"), '', $str_field);
			$string.= $str_field;
		}
		$string.="</table>";
		return $string;
	}
	/**
	 * Sets an ID for this fieldset using the fieldset label.
	 */
	function set_id() {
		$this->id = str_replace(' ', '-', $this->label);
		$this->id = strtolower($this->id);
	}
	/**
	 * Gets the ID for this fieldset
	 * @return $id The fieldset ID.
	 */
	public function id() {
		return $this->id;
	}
	/**
	 * Passes the fieldset to a Validator object and sets the returned
	 * fieldset with validation errors. The method then runs the method 
	 * Validator->error_count(). 
	 * @return array And array of Form\Field objects.
	 */
	public function validate() {
		if(count($_POST) > 0) {
			// Create a new validator object for this fieldset.
			$this->validator = new Validator($this->fieldset);
			$this->fieldset = $this->validator->get_fieldset();
			$this->error_count = $this->validator->error_count(); 
		}
	}
	/**
	 * Creates a new Field obect and populates it with the passed parameters.
	 * @param string  $id          An id for this field
	 * @param boolean $required    True if this is a required field.
	 * @param string  $type        Indicates the field's data type (defaults to text)
	 * @param string  $label       A display label for this field.
	 * @param string  $placeholder An input placeholder for this field.
	 * @param array   $conditions  An array of validation conditions for this field.
	 * @param array   $options     An array of option values for this field (select, radio and checkbox types).
	 * @param string  $message     A display message providing user information about this field.
	 */
	public function set_field(
		$id, $required=true, $type='text', 
		$label=null, $placeholder=null, 
		$conditions=array(), $options=array(), 
		$message=null
	) {
		$this->fieldset[$id] = new Field($id, $required, $type, $label, $placeholder, $conditions, $options, $message);
	}
	/**
	 * Adds or replaces a field object. 
	 * @param \TeamRad\Form\Field $field An instantiated Form\Field
	 */
	public function add_field($field) {
		$this->fieldset[$field->id()] = $field;
	}
	/**
	 * Set the field with $id to required if it exists 	
	 * @param string  $id       The field's id.
	 * @param boolean $required Set to true if this field is required.
	 */
	public function set_required($id=null, $required=true) {
		if(isset($this->fieldset[$id]))
			$this->fieldset[$id]->set_required((boolean) $required);
	}
	/**
	 * Set's the field with $id's data type to $type.
	 * @param string $id   The field's id.
	 * @param string $type The field's data type.
	 */
	public function set_type($id, $type="text") {
		if(isset($this->fieldset[$id]))
			$this->fieldset[$id]->set_type($type);
	}
	/**
	 * Set the display label for the field with $id.
	 * If no $id is passed then the fieldset's display label is set to $label.	
	 * @param string $id    The field's id or null if you want to set a label for the fieldset.
	 * @param string $label The display label for the field.
	 */
	public function set_label($id=null, $label) {
		if(!$id) {
			$this->label = $label;
			$this->set_id();
			return;
		}
		if(isset($this->fieldset[$id]))
			$this->fieldset[$id]->set_label($label);
	}
	/**
	 * Set the input placeholder for the field with $id.
	 * @param string $id          The field's id.
	 * @param string $placeholder Input placeholder text for this field.
	 */
	public function set_placeholder($id=null, $placeholder) {
		if(isset($this->fieldset[$id]))
			$this->fieldset[$id]->set_placeholder($placeholder);
	}
	/**
	 * Set a display message for the field with $id. If no $id 
	 * is passed then the fieldset's display message will be set
	 * to $message.
	 * @param string $id      The id for the field, or null if you want to label the fieldset.
	 * @param string $message The display message you want to set.
	 */
	public function set_message($id=null, $message) {
		if(!$id) {
			$this->message = $message;
			return;	
		}
		if(isset($this->fieldset[$id]))
			$this->fieldset[$id]->set_message($message);
	}
	/**
	 * Set an $options array for a field with $id.
	 * @param string $id      The id for the field whose options you want to set.
	 * @param array  $options An array containing option value pairs for the field.	
	 */
	public function set_options($id, $options) {
		if(isset($this->fieldset[$id]))
			$this->fieldset[$id]->set_options($options);
	}
	/**
	 * Sets the value or values to $value for the field with $id.
	 * @param string $id    The id for the field whose value you want to set.
	 * @param string|array $value The value (string) or values(array) you want to set.
	 */
	public function set_value($id, $value) {
		if(isset($this->fieldset[$id]))
			$this->fieldset[$id]->set_value($value);
	}
	/**
	 * Loops through each fieldset and sets $values
	 * @param Array $values An array of key values to set.
	 */
	public function set_values($values) {
		if (!count($values))
			return;
		// Loop through each field set its $_POST value
		foreach($this->fieldset as $field) {
			if (isset($values[$field->id()]))
				$field->set_value($values[$field->id()]);
		}
		$this->validate();
	} //end set_POST_values()
	/**
	 * Loops through the fieldset and returns their values in an array
	 */
	public function get_values() {
		$values = [];
		// Loop through each field get its value
		foreach($this->fieldset as $field) {
			$values[$field->id()] = $field->value();
		}
		return $values;
	} //end get_values()
	/**
	 * This method allows you to specify a custom Mustache template
	 * for this fieldset's form input. If set, this will override 
	 * the default fieldset form template.
	 * @param sting $template_name The name of the custom template.
	 */
	public function set_tmpl_input($template_name) {
		$template = Helpers\mustache_harcode();
	}
	/**
	 * This method allows you to specify a custom Mustache template
	 * for displaying this fieldset's output. If set, this will 
	 * override the default fieldset output template.
	 * @param string $template_name The name of the custom template.
	 */
	public function set_tmpl_output($template_name) {}
	/**
	 * Gets the field with $id if set. If no $id is set
	 * then this method will get the entire fieldset and 
	 * return it as an array.
	 * returns the entire fieldset in an array. If a value is supplied for 
	 * $key, the method will return the field with that id only.
	 * @todo Review the inclusion of $key versus $id. It's not exactly clear why both are included. 
	 * @param  string $id 	The $id of the fieldset you want to get, or null to get the fieldset.
	 * @param  String $key 	The id of a specific field if you just want to get that. 
	 * @return array     The field or fieldset array.
	 */	
	public function get($id=null, $key=null) {
		if(isset($this->fieldset[$id]))
			return $this->fieldset[$id]->get();
		if(!$id) {
			$fields = array();
			$fields["id"] = $this->id;
			$fields["name"] = $this->label;
			$fields["message"] = $this->message;
			$fields["fields"] = $this->get_field_ids();
			$fields["fieldset"] = $this->get_fields($key);
			return $fields;
		}
	}
	/**
	 * Loops through the fieldset and runs each field's get() method and 
	 * returns the entire fieldset in an array. If a value is supplied for 
	 * $key, the method will return the field with that array key only.
	 * @param  String $key The array key for a specific field.
	 * @return array An array containing all of the fields in the set.
	 */
	public function get_fields($key = null) {
		$fields = array();
		foreach($this->fieldset as $id => $value) {
			if($key) {
				$fields[$id] =$this->fieldset[$id]->get();
			} else {
				$fields[] =$this->fieldset[$id]->get();
			}
		} //end foreach($id)
		return $fields;
	}
	/**
	 * Returns the number of validation errors found in this fieldset.
	 * @return int The number of fieldset validation errors.
	 */
	public function error_count() {
		return $this->error_count;
	}
	/**
	 * Get the fieldset's display label.
	 * @return string The fieldset's display label.
	 */
	public function label() {
		return $this->label;
	}
	/**
	 * Get the fieldset's display message
	 * @return string The fieldset's display message.
	 */
	public function message() {
		return $this->message;
	}
	/**
	 * Returns an array of this fieldset's field ids.
	 * @return array     An array containing the fieldset's field id's.
	 */
	public function get_field_ids() {
		$field_ids = array();
		foreach($this->fieldset as $id => $value) {
			$field_ids[] = $id; 	
		} //end foreach($id)
		return $field_ids;
	}
	/**
	 * Set a minimum (numeric) allowed value for the field with $id.
	 * @param  string $id  The id of the field.
	 * @param  int|float $val The minimum value allowed for the field.
	 */
	public function cnd_minval($id, $val) {
		if(isset($this->fieldset[$id]))
			$this->fieldset[$id]->cnd_minval($val);
	}
	/**
	 * Set a maximum (numeric) allowed value for the field with $id.
	 * @param  string $id  The id of the field.
	 * @param  int|float $val The maximum value allowed for the field.
	 */
	public function cnd_maxval($id, $val) {
		if(isset($this->fieldset[$id]))
			$this->fieldset[$id]->cnd_maxval($val);
	}
	/**
	 * Set minimum and maximum (numeric) allowed values for the field with $id.
	 * @param  string $id  The id of the field.
	 * @param  int|float $val1 The first value to set as min/max for this field.
	 * @param  int|float $val2 The second value to set as min/max for this field.
	 */
	public function cnd_btwval($id, $val1, $val2) {
		if(isset($this->fieldset[$id]))
			$this->fieldset[$id]->cnd_btwval($val1, $val2);
	}
	/**
	 * Set that a field with $id's value must be positive.
	 * @param  string $id  The id of the field.
	 * @param  boolean $pos True if the field's value must be positive.
	 */
	public function cnd_pos($id, $pos=true) {
		if(isset($this->fieldset[$id]))
			$this->fieldset[$id]->cnd_pos($pos);
	}
	/**
	 * Set that a field with $id's value must be neagtive.
	 * @param  string $id  The id of the field.
	 * @param  boolean $neg True if the field's value must be negative.
	 */
	public function cnd_neg($id, $neg=true) {
		if(isset($this->fieldset[$id]))
			$this->fieldset[$id]->cnd_neg($neg);
	}
	/**
	 * Set the field with $id's exact length to $val.
	 * @param  string $id  The id of the field.
	 * @param  int $val The exact length required for this field's value.
	 */
	public function cnd_len($id, $val) {
	if(isset($this->fieldset[$id]))
			$this->fieldset[$id]->cnd_len($val);
	}
	/**
	 * Set the field with $id's minimum length to $val.
	 * @param  string $id  The id of the field.
	 * @param  int $val The minimum length required for this field's value.
	 */
	public function cnd_minlen($id, $val) {
		if(isset($this->fieldset[$id]))
			$this->fieldset[$id]->cnd_minlen($val);
	}
	/**
	 * Set the field with $id's maximum length to $val.
	 * @param  string $id  The id of the field.
	 * @param  int $val The maximum length required for this field's value.
	 */
	public function cnd_maxlen($id, $val) {
		if(isset($this->fieldset[$id]))
			$this->fieldset[$id]->cnd_maxlen($val);
	}
	/**
	 * Set the field with $id's minimum and maximum lengths to $val1 and $val2.
	 * @param  string $id  The id of the field.
	 * @param  int $val1 The min/max length allowed for this field.
	 * @param  int $val2 The min/max length allowed for this field.
	 */
	public function cnd_btwlen($id, $val1, $val2) {
		if(isset($this->fieldset[$id]))
			$this->fieldset[$id]->cnd_btwlen($val1, $val2);
	}
	/**
	 * Specify a string which the field with $id's value must contain.
	 * @param  string $id  The id of the field.
	 * @param  string $val A string which must be contained in the field's value.
	 */
	public function cnd_contains($id, $val) {
		if(isset($this->fieldset[$id]))
			$this->fieldset[$id]->cnd_contains($val);
	}
	/**
	 * Specify a string which the field with $id's value must not contain.
	 * @param  string $id  The id of the field.
	 * @param  string $val A string which must not be contained in the field's value.
	 */
	public function cnd_not($id, $val) {
		if(isset($this->fieldset[$id]))
			$this->fieldset[$id]->cnd_not($val);
	}
} // end class
?>