<?php
/**
 * This is the database object used in the ShareIT 
 * website. If the db credentials are changed, just
 * update them in the class constructor.
 */
namespace TeamRad;
/**
 * Use this class in place of PHP's PDO object. It contains the correct
 * configuration for establishing a database connection with the ShareIT
 * website database. 
 */
class DB extends \PDO {
	/**
	 * Connects to the database.
	 */
	public function __construct() {
		$servername = "localhost";
		$db = "shareIT";
		$username = "root";
		$password = "";
		parent::__construct('mysql:host='.$servername.';dbname='.$db, $username, $password);
	} // end __construct()
} // end Class