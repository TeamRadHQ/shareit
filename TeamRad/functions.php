<?php
/**
 * These are helper classes used by the TeamRad classes. 
 */
/**
 * Returns $prefix with an underscore appended.
 * @param  string $prefix The prefix.
 * @return string         The prefix with an underscore.
 */
function prefix($prefix=null) {
		if($prefix)
			$prefix.="_";
		return $prefix;
}
/**
 * Takes two values (numeric), sorts them as "min" and "max"
 * and returns these in an array.
 * @param  int|float $val1 The first value you want to sort.
 * @param  int|float $val2 The second value you want to sort. 
 * @return array       	An array containing ["min", "max"].
 */
function minmax($val1, $val2) {
	if ($val1 < $val2 || $val1 == $val2) { //Val1 is min
		return array("min" => $val1, "max" => $val2);
	} else { // Val2 is min
		return array("min" => $val2, "max" => $val1);
	}
}// end minmax()


?>
