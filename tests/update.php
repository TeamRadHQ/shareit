<?php
include('autoload.php');
use TeamRad\DB as DB;
$page_title = "Share IT - Update Queries";
add_head("$page_title");
?>

<?php 
try {
	$conn = new DB();
	// set the PDO error mode to exception
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	echo "success!";
} catch(PDOException $e) {
	echo "Failed: ", $e->getMessage();
}
echo "<h1> Landlord Update </h1>";
// Update statement for Landlord table
$update = "UPDATE landlord
	SET firstName = :firstName,
		lastname = :lastName,
		countryCode = :countryCode,
		city = :city,
		mob1 = :mob1,
		mob2 = :mob2
	WHERE landlordID = :id";
// Set values to bind
$values = [
	":id"			=> 3,
	":firstName"	=> "Bill",
	":lastName"		=> "Johnson",
	":countryCode"	=> "AU",
	":city"			=> "Melbourne",
	":mob1"			=> "0404131178",
	":mob2"			=> "0412345484"	
];
// Prepare the query
$query = $conn->prepare($update);
// Execute
$query->execute($values);

echo "<h1> User Email Update </h1>";
// Update statement for updating user email
$update = "UPDATE user
	SET email = :email
	WHERE userID = :id";
// Set values to bind
$values = [
	":id"			=> 1,
	":email"		=> "test@test.com"
];
// Prepare the query
$query = $conn->prepare($update);
// Execute
$query->execute($values);

echo "<h1> User Password Update </h1>";
// Update statement for updating user password
$update = "UPDATE user
	SET password = :password,
		clue = :clue
	WHERE userID = :id";
// Set values to bind
$values = [
	":id"			=> 1,
	":password"		=> "password",
	":clue"			=> "the Default"
];
// Prepare the query
$query = $conn->prepare($update);
// Execute
$query->execute($values);


?>

<?php add_JS();?>
</body>
</html>