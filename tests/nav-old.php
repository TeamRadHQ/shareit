<?php

$nav_items = ['Home', 'Search', 'Login'];
function nav_li($title, $href) {
    $class='';
    // Get the page and remove .php
    $page = basename($_SERVER['PHP_SELF']);
    if ($href===$page)
        $class = "active ";
    $nav_li = '<li class="'.$class.'"><a href="'.$href.'">'.$title.'</a></li>';
    return $nav_li;
} 

function nav() {
    $nav = '<nav class="navbar navbar-default main container">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">ShareIT</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">';
        // Add Main Navs here
        // 
    $nav.='</ul>
        <ul class="nav navbar-nav navbar-right">';
    // Add nav dropdown here
    $nav.=nav_li('Home', 'index.php');
    $nav.=nav_li('Countries', 'country.php');
    $nav.=nav_li('Locations', 'location.php');
    $nav.=nav_li('Properties', 'properties.php');

    $nav.='     </ul>
        <ul class="nav navbar-nav navbar-right">';
    // In not logged in
    if (!isset($_SESSION["userID"])) {
        $nav.='<li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Login <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li>
                    <form method="post" action="login.php" class="form-group">
                        <input class="form-control" id="email" name="email" type="text" placeholder="Email">
                        <input class="form-control" id="password" name="password" type="password" placeholder="Password">
                        <button type="submit" class="form-control btn-block btn-primary">Login</button>
                    </form>
                </li>
            </ul>
        </li>';
    } 
    // If logged in
    if (isset($_SESSION["userID"])) {
        $nav.='<li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">My Settings<span class="caret"></span></a>
            <ul class="dropdown-menu">';
            $nav.=nav_li('My Profile', 'profile.php');
            $nav.=nav_li('My Properties', 'admin.php');
            $nav.=nav_li('New Listing', 'edit.php');
            $nav.='<li role="separator" class="divider"></li>';
            $nav.=nav_li('Logout', 'logout.php');
        $nav.='</ul>
        </li>';
    }// end if(logged in)
    // Close nav here
    $nav.='     </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>';
}
?>
<nav class="navbar navbar-default main container">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
		<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        	<span class="sr-only">Toggle navigation</span>
        	<span class="icon-bar"></span>
        	<span class="icon-bar"></span>
        	<span class="icon-bar"></span>
      	</button>
      	<a class="navbar-brand" href="#">ShareIT</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    	<ul class="nav navbar-nav">
            <?php nav_li('Home', 'index.php');?>
            <?php nav_li('Countries', 'country.php');?>
            <?php nav_li('Locations', 'location.php');?>
            <?php nav_li('Properties', 'properties.php');?>
    	</ul>
        <ul class="nav navbar-nav navbar-right">
<?php
if (!isset($_SESSION["userID"])) {
?> 
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Login <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li>
                        <form method="post" action="login.php" class="form-group">
                            <input class="form-control" id="email" name="email" type="text" placeholder="Email">
                            <input class="form-control" id="password" name="password" type="password" placeholder="Password">
                            <button type="submit" class="form-control btn-block btn-primary">Login</button>
                        </form>
                    </li>
                </ul>
            </li>
  <?php 
}

if (isset($_SESSION["userID"])) {
?>
    	   	<li class="dropdown">
		   		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">My Settings<span class="caret"></span></a>
				<ul class="dropdown-menu">
                    <?php nav_li('My Profile', 'profile.php');?>
                    <?php nav_li('My Properties', 'admin.php');?>
                    <?php nav_li('New Listing', 'edit.php');?>
                    <li role="separator" class="divider"></li>
                    <?php nav_li('Logout', 'logout.php');?>
				</ul>
    	   	</li>
<?php 
}
?> 
    	</ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>