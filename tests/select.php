<?php
include('autoload.php');
use TeamRad\DB as DB;
$page_title = "Share IT";
add_head("$page_title");
?>
<body class="container">
<h1> <?php echo $page_title; ?> </h1>

<?php 
// Database Variables
$servername = "localhost";
$db = "shareIT";
$username = "root";
$password = "";
// Try and connect to the database
// require('DB.php');
try {
	$conn = new DB();
	// set the PDO error mode to exception
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	echo "success!";
} catch(PDOException $e) {
	echo "Failed: ", $e->getMessage();
}

echo "<h1>Specific Query</h1>";
$sql = "SELECT property.costperday as dailyRate, 
			property.propertyID as ID, 
			location.locationID, 
			landlord.landlordID, 
			country.countryCode, 
			property.comments, 
			property.bedrooms,
			property.bedSingle,
			property.bedDouble,
			property.toilet,
			property.kitchen,
			property.laundry,
			property.street,
			property.suburb, 
			property.postcode,
			country.CommonName as country,
			location.LocationName as location,
			CONCAT(landlord.firstName, ' ', landlord.lastName) as landlord
		FROM property, location, landlord, country 
		WHERE property.LocationID = location.LocationID
			AND property.LandlordID = landlord.LandlordID
			AND location.CountryCode = country.CountryCode";
$result = $conn->query($sql);
echo '<pre>', print_r($result->fetch(DB::FETCH_ASSOC)), '</pre>';

echo "<h1>user</h1>";
$sql = "SELECT * FROM user";
$result = $conn->query($sql);
echo '<pre>', print_r($result->fetch(DB::FETCH_ASSOC)), '</pre>';

echo "<h1>property</h1>";
$sql = "SELECT * FROM property";
$result = $conn->query($sql);
echo '<pre>', print_r($result->fetch(DB::FETCH_ASSOC)), '</pre>';

echo "<h1> Landlord</h1>";
$sql = "SELECT * FROM landlord";
$result = $conn->query($sql);
echo '<pre>', print_r($result->fetch(DB::FETCH_ASSOC)), '</pre>';

echo "<h1> Country </h1>";
$sql = "SELECT * FROM country";
$result = $conn->query($sql);
echo '<pre>', print_r($result->fetch(DB::FETCH_ASSOC)), '</pre>';

echo "<h1> Location </h1>";
$sql = "SELECT * FROM location";
$result = $conn->query($sql);
echo '<pre>', print_r($result->fetch(DB::FETCH_ASSOC)), '</pre>';

echo "<h1>Country </h1>";
$country = "";
$result = $conn->qry_country();
echo "<h1> Location Table </h1>";
$result = $conn->qry_location();
?>
<select>
<?php
while ($row = $result->fetch(DB::FETCH_ASSOC)) {
	echo '<option value="'.$row["countrycode"].'">' . $row["country"]. '</option>' . $row["country"] . '</a></small></h3></td></tr>';
	// echo '<pre>', print_r($row), '</pre>';
}
?>
</select>
<?php
echo "<h1> Location Table </h1>";
$result = $conn->qry_location();
?>
<select>
<?php
while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
	echo '<option value="'.$row["locationID"].'">' . $row["location"]. '</option>' . $row["country"] . '</a></small></h3></td></tr>';
	// echo '<pre>', print_r($row), '</pre>';
}
?>
</select>

<?php add_JS();?>
</body>
</html>