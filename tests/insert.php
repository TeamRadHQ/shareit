<?php
include('autoload.php');
use TeamRad\DB as DB;
$page_title = "Share IT - Insert Queries";
add_head("$page_title");
?>
<body class="container">
<h1> <?php echo $page_title; ?> </h1>

<?php 
try {
	$conn = new DB();
	// set the PDO error mode to exception
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	echo "success!";
} catch(PDOException $e) {
	echo "Failed: ", $e->getMessage();
}
?>

<?php add_JS();?>
</body>
</html>