<?php
include('autoload.php');
// include('/app/login-form.php');
$page_title = "Tabs Test";
add_head("$page_title");
?>

<body class="container">
<?php include 'nav.php'; ?>
<header>
<h1> Property <span class="small">1 Smith Street, Smithton</span> 
</h1>
<p> A brief description about the property. </p>
<?php include 'carousel.php'; ?>
</header>

<?php
$tabs = [];
$tabs[] = ['name' => "About", "content" => "<p>This tab could display a general information section and a main picture.</p>"];
// $tabs[] = ['name' => "Gallery", "content" => "<p>This tab could contain more images with captions.</p>"];
$tabs[] = ['name' => "Bookings", "content" => "<p>More detailed information could go here.</p>"];
$tabs[] = ['name' => "Edit", "content" => "<p>This section would only be visible to owners of this item and site administrators.</p>"];

	// echo $article;

?>

<?php
do_tabs($tabs);
?>


<?php add_JS(); ?>
</body>
</html>