<?php
/**
 * The login page handles the login process for regsitered users. 
 * It uses the FormLogin class to collect and validate user 
 * credentials before redirecting the user to the admin page. 
 */
namespace ShareIt;
// Create a new session
session_start();

require_once('autoload.php');

// use Form\FormLogin as Login;
//Create the login form
$form    = new \ShareIt\Form\FormLogin();
$form->set_POST_values();
$db = new DB;

// Set the user to check login against the database
$user = check_login();
// If there are no form errors, check the user's login
if (!$form->errors()) {
	// check the user's login with the database and set to variable
	// If the user if valid, create a login session and redirect
	if ($user) {
		// Add userID, name and role to the session
		$_SESSION["userID"] = $user["userID"];
		$_SESSION["userName"] = $user["userName"];
		$_SESSION["roleID"] = $user["roleID"];
		// Kill the page after redirecting the user to the admin area
		header("Location: /admin.php");
		die();
	} // end if($user)	
} // end if($form->errorrs)
// Otherwise, load the page

// Set the page title
$page_title = "Share IT - Login Page";
// Add the page header
add_head("$page_title");
?>
<section class="col-md-10 col-md-push-1">

<?php
if (isset($_GET["registered"])) {
	echo "<p><strong>Success:</strong> You have now registered, please log in.</p>";
}
// If the user's login form submission is invalid, tell them.
if ( !$user && isset($_POST["email"]) && isset($_POST["email"]) ) 
	echo '<p> User email or password don\'t match.</p>';
// Render  new login form
$form->render();
?>
</section>

<?php add_JS();?>
</body>
</html>