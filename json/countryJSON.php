<?php
/**
 * This file outputs JSON from the database containing 
 * countries and locations. This is used by front-end 
 * JavaScript methods for page manipulation.
 */
require_once($_SERVER["DOCUMENT_ROOT"].'/ShareIt/DB.php');
use \ShareIt\DB as DB;
// Select query for getting coutnries and locations
$sql = "SELECT c.commonName as country, c.countrycode, l.locationName as location, l.locationID 
	FROM country c, location l
	WHERE c.countrycode = l.countrycode
	ORDER BY country, location";  
// Database object
$db = new DB;
// Database results
$results = $db->query($sql);
// Set blank country code
$code="";
// Add each row to location array
while($row = $results->fetch(DB::FETCH_ASSOC)) {
	extract($row);
	// If it's a new country
	if($countrycode!=$code) {
		// Change the code and add its name
		$code=$countrycode;
		$locations[$code]["name"] = $country; 
		$locations[$code]["id"] = $code; 
	}
	$locations[$code]["locations"][$locationID] = [
		"name" => $location,
		"id" => $locationID]; 
}
echo json_encode($locations);
?>
