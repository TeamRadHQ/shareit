<?php
/**
 * The admin page allows registered uses to view their user profile and 
 * any properties which they have added. If a user is not logged in and 
 * they try to access this page, they will be redirect to the index page.
 */
namespace ShareIt;
require_once('autoload.php');

use TeamRad\Helpers as Helpers;
// Create a new session
session_start();
// Redirect unauthorised users to login
redirect_login();

// Get a database connection
$db = new DB;
$propertyID = set_propertyID();
$pictureID = set_pictureID();
// Otherwise, load the page
// Set the page title
$page_title = "Delete";
$delete_flag =  delete_flag();
// Set the property array
if ($propertyID) {
	$property = get_property($propertyID);
	$address = $property["street"] . ", " . $property["suburb"];
	$page_title .= " Property <small>" . $address . "</small>";
}
// Add the pictureID and its href
if ($pictureID) {
	$property["pictureID"] = $pictureID;
	$property["href"] = single_img($pictureID);
}

// Add the page header
add_head("$page_title");
?>
<section class="col-md-9">
<?php 
	$mustache = template();
	// Show the property delete section
	if ($propertyID && !$pictureID) {
		if (! isset($delete_flag)) {
			echo $mustache->render('delete_property_warning', $property); 
		} else {
			delete_property($propertyID);
			success_message("","", "You have deleted this property.");
		}
	// Otherwise show the picture delete section
	} else if ($pictureID) {
		if ( ! $delete_flag) {
			echo $mustache->render('delete_image_warning', $property); 
		} else {
			delete_img($pictureID, $property["href"]);
			success_message("","", "You have deleted this image.");
		}
	}
?>
</section>
<?php
// Show current images for property
admin_property_img();
?>

<?php add_JS();?>
</body>
</html>