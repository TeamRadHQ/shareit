<?php
/**
 * The country page is supposed to display 
 * properties in a particular country. However,
 * it is probably going to be removed and 
 * incorporated with location.php.
 */
namespace ShareIt;
include('autoload.php');
session_start();
$page_title = "Countries";
add_head("$page_title");
?>

<?php add_JS();?>
</body>
</html>