<?php
/**
 * The admin page allows registered uses to view their user profile and 
 * any properties which they have added. If a user is not logged in and 
 * they try to access this page, they will be redirect to the index page.
 */
namespace ShareIt;
require_once('autoload.php');
use TeamRad\Helpers as Helpers;
// Get a database connection
$db = new DB;
$page_title = "Search by Location or Country";
$locationID = set_locationID();
$countrycode = set_countrycode();
// If this is a location search
if($locationID) {
	// Set the page title and get properties
	$location_name = location_name($locationID);
	$page_title = "Properties in $location_name";
	$properties = location_properties($locationID);
}
// If this is a country search
if($countrycode) {
	// Set the page title and get properties
	$country_name = country_name($countrycode);
	$page_title = "Properties in $country_name";
	$properties = country_properties($countrycode);
}
// Add the page header
add_head("$page_title");
?>
<pre><?php 

?></pre>
<section class="col-md-12 jumbotron">
	<strong> Search Locations: </strong> 
	<span id="locationSelectFront">	
		<select id="countryFront">
			<option>Select a country</option>
		</select>
		<select id="locationFront">
		</select>
	</span>
</section>
<section class="col-md-9">
<?php 
	if ( isset($properties) ) {
 		property_table($properties); 
	} else {
		echo "<p> Please select a location or country to view its properties.</p>";
	}
?>
</section>
<?php add_JS();?>
<script>
$(document).ready(function() {
	// Set the countryCode select to redirect user to another country
	$('#countryFront').change(function() {
	  	// set the window's location property to the value of the option the user has selected
		window.location.href = "http://<?php echo $_SERVER['SERVER_NAME']; ?>/location.php?countrycode=" + $(this).val();
	});
	// Set the countryCode select to redirect user to another country
	$('#locationFront').change(function() {
	  	// set the window's location property to the value of the option the user has selected
		window.location.href = "http://<?php echo $_SERVER['SERVER_NAME']; ?>/location.php?locationID=" + $(this).val();
	});
});
</script>

</body>
</html>