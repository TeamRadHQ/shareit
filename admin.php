<?php
/**
 * The admin page allows registered uses to view their user profile and 
 * any properties which they have added. If a user is not logged in and 
 * they try to access this page, they will be redirect to the index page.
 */
namespace ShareIt;
require_once('autoload.php');
use TeamRad\Helpers as Helpers;
// Create a new session
session_start();
// Redirect unauthorised users to login
redirect_login();
// Get a database connection
$db = new DB;
// Otherwise, load the page
// Set the page title
$page_title = "Admin";
// Add the page header
add_head("$page_title");
?>
<section class="col-md-9">
	<h3> Your Properties </h3>
	<?php property_table(); ?>
</section>
<section class="col-md-3">
	<h3> <a href="profile.php">Your Profile <small><span class="glyphicon glyphicon-pencil"></span></small></a> </h3>
	<?php profile_table(); ?>
</section>
<?php add_JS();?>
</body>
</html>