<?php
/**
 * This is the autoload script. It loads all of the required assets for 
 * the ShareIT website. It starts by requiring vendor/autoload which is a
 * PHP Composer autoloader for including third party modules. It then registers
 * a class autoloader which includes all namespaced classes in this project. 
 * Finally, helper classes for TeamRad and ShareIT assets are required.
 */

// Third party module autoloader
require_once('vendor/autoload.php');
?>
