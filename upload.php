<?php
/**
 * This page handles image uploads to the ShareIT website.
 * It uses the FormUpload Class to upload files and create 
 * records in the picture database. 
 * @uses \TeamRad\Form\FormUpload
 */
namespace ShareIt;
// Autoload scripts
require_once('autoload.php');

use TeamRad\Helpers\Opt as Opt; 
use \ShareIt\Form\FormUpload as Upload;

// Start a new session
session_start();
// Redirect unauthorised users to login
redirect_login();
// Get a database connection
$db = new DB;

// Load the page
// Set the page title
$page_title = "Add Property Photos";
// Add the page header
add_head("$page_title");
?>

<section class="col-md-9">
<?php
success_message("The image was updated.", "The image was added.");
$form = new Upload();
$form->render(); 
?>
</section>

<?php
// Show current images for property
admin_property_img();
?>

<?php add_JS();?>

<script>
$(document).ready(function() {
	// Set the propertyID select to redirect user to another property
	$('#propertyID').change(function() {
	  	// set the window's location property to the value of the option the user has selected
		window.location.href = "http://<?php echo $_SERVER['SERVER_NAME']; ?>/upload.php?propertyID=" + $(this).val();
	});
});
</script>
</body>
</html>