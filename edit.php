<?php
/**
 * The edit page allows a registered user to add a new property
 * or edit the details of any of their existing properties. 
 * It uses the FormProperty class to collect and validate infomation
 * about a property.
 * @uses \TeamRad\Form\FormProperty
 */
namespace ShareIt;
require_once('autoload.php');

// Add Helpers
use TeamRad\Helpers\Opt as Opt; 
use \ShareIt\Form\FormProperty as Property;

// Create a new session
session_start();
// Redirect unauthorised users to login
redirect_login();
// Get a database connection
$db = new DB;
// Otherwise, load the page
// Set the page title
if (isset($_GET["propertyID"])) {
	$page_title = "Edit Property";
} else {
	$page_title = "Add a New Property";
}

// Add the page header
add_head("$page_title");
?>

<section class="col-md-9">
<?php
success_message("Your property has been updated!", "A new property has been updated!");
$form = new Property();
$form->render(); 
?>
</section>
<?php
// Show current images for property
admin_property_img();
add_JS();?>
</body>
</html>