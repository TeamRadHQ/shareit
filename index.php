<?php
/**
 * The admin page allows registered uses to view their user profile and 
 * any properties which they have added. If a user is not logged in and 
 * they try to access this page, they will be redirect to the index page.
 */
namespace ShareIt;
require_once('./vendor/autoload.php');
use TeamRad\Helpers as Helpers;
// Get a database connection
$db = new DB;
// Get some random properties
$properties = random_properties(5);
// Add the page header
add_head("Welcome to ShareIt");
?>
<section class="col-md-12 jumbotron">
	<p> Short-term accommodation rentals.</p>
	<ul>
		<li><small><a class="" href="location.php">Search for properties by country or location.</a></small></li>
		<li><small><a class="" href="register.php">Register as a Landlord.</a></small></li>
	</ul>
</section>
<section class="col-md-9">
	<h3>View Properties</h3> 
<?php property_table($properties); ?>
</section>
<?php add_footer(); ?>
<?php add_JS();?>
</body>
</html>