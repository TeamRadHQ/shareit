<?php
/**
 * The logout page clears the session array, destroys the 
 * session and redirects the user to the front page.
 */
namespace ShareIt;
require_once('autoload.php');
// Get the session of the logged in user
session_start();
// Clear the session variables
$_SESSION = array();
// Destroy the session
session_destroy();
// Redirect user
header("Location: /");
die();
?>