<?php
/**
 * The property page serves a single property listing
 * to the user.
 */
namespace ShareIt;
include('autoload.php');
session_start();

$propertyID = set_propertyID();
$property = get_property($propertyID);

// Otherwise, get the images and set the propertyID.
$images["rows"] = get_property_images();
$images["propertyID"] = $propertyID;
$mustache = template();

$page_title = $property["street"] . ', ' . $property["suburb"] . ' ' . $property["country"];
add_head("$page_title");
?>
<section class="jumbotron">
<p> <?php echo $property["comments"]; ?></p>
</section>
<section class="row">
	<?php echo $mustache->render('property_slideshow', $images); ?>
	<?php echo $mustache->render('property_map', $property); ?>
</section>
<section class="row">
	<?php echo $mustache->render('property_booking', $property); ?>
	<?php echo $mustache->render('property_location', $property); ?>
</section>
<section class="row">
<?php echo $mustache->render('property_bedroom', $property); ?>
<?php echo $mustache->render('property_facilities', $property); ?>
</section>
<?php add_JS();?>
</body>
</html>