<?php
/**
 * This is a test file for testing 
 * out features and functionality.
 */
namespace ShareIT;
include('autoload.php');
session_start();
$page_title = "Test Page";
add_head("$page_title");

$sql = "SELECT c.commonName as country, c.countrycode, l.locationName as location, l.locationID 
	FROM country c, location l
	WHERE c.countrycode = l.countrycode
	ORDER BY country, location";  


?>

<p id="locationSelectFront">
	<select id="countryFront">
		<option>Select a country</option>
	</select>
	<select id="locationFront">
	</select>
</p>
<?php add_JS();?>
<script>
$(document).ready(function() {
	// Set the countryCode select to redirect user to another country
	$('#countryFront').change(function() {
	  	// set the window's location property to the value of the option the user has selected
		window.location.href = "http://shareIt.org/location.php?countrycode=" + $(this).val();
	});
	// Set the countryCode select to redirect user to another country
	$('#locationFront').change(function() {
	  	// set the window's location property to the value of the option the user has selected
		window.location.href = "http://shareIt.org/location.php?locationID=" + $(this).val();
	});
});
</script>
</body>
</html>