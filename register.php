<?php
/**
 * The Profile page allows a registered user to 
 * update their information. It uses the FormProfile
 * class to collect user data. If a user is not logged in
 * they will be redirected to the login page.
 * @uses \ShareIt\Form\FormProfile
 */
namespace ShareIt;
namespace ShareIT;
require_once('autoload.php');
require_once($_SERVER["DOCUMENT_ROOT"].'/ShareIt/Form/FormRegistration.php');
// Add Helpers
use TeamRad\Helpers\Opt as Opt; 
use \ShareIt\Form\FormRegistration as Registration;

// Get a database connection
$db = new DB;
// Otherwise, load the page
// Set the page title
$page_title = "User Registration";
// Add the page header
add_head("$page_title");
?>

<section class="col-md-9">
<?php
success_message("Your profile has been updated!", "");
$form = new Registration();
$form->render(); 
?>
</section>
<section class="col-md-3">
</section>

<?php add_JS();?>
</body>
</html>