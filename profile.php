<?php
/**
 * The Profile page allows a registered user to 
 * update their information. It uses the FormProfile
 * class to collect user data. If a user is not logged in
 * they will be redirected to the login page.
 * @uses \ShareIt\Form\FormProfile
 */
namespace ShareIt;
namespace ShareIT;
require_once('autoload.php');

// Add Helpers
use TeamRad\Helpers\Opt as Opt; 
use \ShareIt\Form\FormProfile as Profile;

// Create a new session
session_start();
// Redirect unauthorised users to login
redirect_login();
// Get a database connection
$db = new DB;
// Otherwise, load the page
// Set the page title
$page_title = "Update Profile";
// Add the page header
add_head("$page_title");
?>

<section class="col-md-9">
<?php
success_message("Your profile has been updated!", "");
$form = new Profile();
$form->render(); 
?>
</section>
<section class="col-md-3">
	<h3> <a href="profile.php">Your Profile <small><span class="glyphicon glyphicon-pencil"></span></small></a> </h3>
	<?php profile_table(); ?>
	<p> <a href="#" class="btn-primary btn-lg btn-block">Change your password</a> </p>
</section>

<?php add_JS();?>
</body>
</html>