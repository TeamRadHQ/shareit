# README

ShareIt is a website for listing places for short term accommodation. 

[View a Live Version of the Site](http://shareit.teamradhq.com)

## Development

This has been developed using a set of classes I developed TeamRad/Form which generates user forms, provides field validation, database operations and user output.

### Third Party Tools

* All elements on the page are output using [Mustache PHP](https://github.com/bobthecow/mustache.php) templating.

* The CSS primarily consists of Bootstrap CSS, with very few minor customisations.

* The site makes minimal use of JSON and jQuery to provide a few minor user interactions.