$.getJSON( "/json/countryFrontEndJSON.php", function( data ) {
  	// Parse the DOM objects
	var $location = $('#locationSelectFront');
  	var $countrySelect = $location.children('#countryFront')
  	var $locationSelect = $location.children('#locationFront')

  	// Reset the fields to their initialised state.
  	function reset(data) {
  		// Loop through each country
	  	$.each( data, function( key, countries ) {
	  		// Add it to the country selector
			option = $( "<option/>", { html: countries.name,value: key });
	  		$countrySelect.append(option);
	  		// Loop through each location
	  		$.each(countries.locations, function(key, location) {
				option = $( "<option/>", { html: location.name,value: location.id });
	  			$locationSelect.append(option);
		  	}); // End each countries.locations
	  	}); // End each Data
	  	// Sort the locations alphabetically
	  	sortLocation()
  	}// End reset()

  	// Reset on initialise
  	reset(data);
	// Sets an option & value
  	function setOption(html, value) {
  		option =  $( "<option/>", { html: html, value: value });
  		return option;
  	}
  	// Sorts the location options alphabetically
  	function sortLocation() {
  		// Get the options
	  	var options = $locationSelect.children(); 
	  	// Detach and sort
	  	options.detach().sort(function(a,b) {
	  		var at = $(a).text();
	  		var bt = $(b).text();
	  		return (at>bt)?1:((at <bt))?-1:0;
	  	});
	  	// Add to options
	  	$locationSelect.append(options);
	  	// The default option
		option1 = setOption('Select a location',null)
    	// Prepend defaul
		$locationSelect.prepend(option1);
  	}

	// What happens when a country is selected
	$countrySelect.change(function() { //this occurs when select 1 changes
		// Get the selected country code
    	countryCode = $countrySelect.val()
    	// Get the selected location
    	locationVal = $locationSelect.val()
    	// Clear the location Select Options
    	$locationSelect.html("");
		// If no country is selected
		if (countryCode === "Select a country") {
			// Reset the location field
			reset(data);
		} else {
			// Otherwise, restrict locations to the selected country
			$.each(data[countryCode].locations, function(key, location){
				option = $( "<option/>", { html: location.name,value: location.id });
	  			$locationSelect.append(option);
			}); // end each countryCode			
		} // end if else
		sortLocation(); 
  	});// end each counntrySelectChange 

});// End getJSON